#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/norm.hpp>
#include <glm/gtx/rotate_vector.hpp>

// CPP
#include <iostream>
#include <fstream>

#include <glm/gtx/string_cast.hpp>

// OBJ Model Loader
#include <openGL/objects/loaders/OBJ_Loader.hpp>

// OpenGL Data
#include <openGL/objects/MeshObject.hpp>
#include <openGL/objects/BSpline.hpp>
#include <openGL/rendering/Shader.hpp>
#include <openGL/objects/materials/Material.hpp>

#include <openGL/rendering/ShaderUtil.hpp>

// Renderer
#include <openGL/rendering/Renderer.hpp>

// Material

// Logging
#include <general/Logger.hpp>

#include <openGL/rendering/Camera.hpp>
#include <openGL/objects/ParticleGenerator.hpp>

// Image loading
#include<image_load/stb_image.h>
#include <openGL/rendering/particle_system/ParticleUpdaterFire.hpp>
#include <openGL/rendering/PerspectiveCamera.hpp>
#include <openGL/rendering/SceneGraphRenderer.hpp>
#include "openGL/rendering/particle_system/ParticleGeneratorFire.hpp"
#include "openGL/rendering/particle_system/ParticleRespawnFire.hpp"

struct Win {
    int id;
    int width;
    int height;
};

// Structures
struct Mouse {
    int x;
    int y;
    GLfloat lastX;
    GLfloat lastY;
    bool rightPress;
    int clickIdx;
    bool firstMouse;
};

Mouse myMouse;
Win mainWindow;

// Function prototypes
void initDefaultWindowOptions();

// Callbacks
void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode);

void mouseCallback(GLFWwindow *window, double xpos, double ypos);

void scrollCallback(GLFWwindow *window, double xoffset, double yoffset);

void resizeCallback(GLFWwindow *window, int width, int height);

void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods);

void doMovement(GLFWwindow *window, double deltaTime);

// Flags for events
bool keys[1024];

// Draw by elements or arrays
bool toggleArrays = false;

bool billBoarding = false;

std::vector<CoreParticleGenerator *>
createFireGenerators(const std::string &tex_path, int numOfParticles, glm::vec3 initialPosition);

void GLAPIENTRY debugInfoLog(GLenum source,
                             GLenum type,
                             GLuint id,
                             GLenum severity,
                             GLsizei length,
                             const GLchar *message,
                             const void *userParam) {
//    LOGN("[DEBUG MESSAGE]");
//    LOG("[SOURCE] ");
//    LOGN(source);
//    LOG("[TYPE] ");
//    LOGN(type);
//    LOG("[ID] ");
//    LOGN(id);
//    LOG("[SEVERITY] ");
//    LOGN(severity);
//    LOG("[MESSAGE] ");
//    LOGN(message);
// ignore non-significant error/warning codes
    if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

    std::cout << "---------------" << std::endl;
    std::cout << "Debug message (" << id << "): " << message << std::endl;

    switch (source) {
        case GL_DEBUG_SOURCE_API:
            std::cout << "Source: API";
            break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
            std::cout << "Source: Window System";
            break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER:
            std::cout << "Source: Shader Compiler";
            break;
        case GL_DEBUG_SOURCE_THIRD_PARTY:
            std::cout << "Source: Third Party";
            break;
        case GL_DEBUG_SOURCE_APPLICATION:
            std::cout << "Source: Application";
            break;
        case GL_DEBUG_SOURCE_OTHER:
            std::cout << "Source: Other";
            break;
    }
    std::cout << std::endl;

    switch (type) {
        case GL_DEBUG_TYPE_ERROR:
            std::cout << "Type: Error";
            break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            std::cout << "Type: Deprecated Behaviour";
            break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            std::cout << "Type: Undefined Behaviour";
            break;
        case GL_DEBUG_TYPE_PORTABILITY:
            std::cout << "Type: Portability";
            break;
        case GL_DEBUG_TYPE_PERFORMANCE:
            std::cout << "Type: Performance";
            break;
        case GL_DEBUG_TYPE_MARKER:
            std::cout << "Type: Marker";
            break;
        case GL_DEBUG_TYPE_PUSH_GROUP:
            std::cout << "Type: Push Group";
            break;
        case GL_DEBUG_TYPE_POP_GROUP:
            std::cout << "Type: Pop Group";
            break;
        case GL_DEBUG_TYPE_OTHER:
            std::cout << "Type: Other";
            break;
    }
    std::cout << std::endl;

    switch (severity) {
        case GL_DEBUG_SEVERITY_HIGH:
            std::cout << "Severity: high";
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            std::cout << "Severity: medium";
            break;
        case GL_DEBUG_SEVERITY_LOW:
            std::cout << "Severity: low";
            break;
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            std::cout << "Severity: notification";
            break;
    }
    std::cout << std::endl;
    std::cout << std::endl;

//    fprintf(stderr,
//            "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
//            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
//            type, severity, message);
}

void initDefaultWindowOptions() {
    myMouse.firstMouse = true;

    mainWindow.width = 1024;
    mainWindow.height = 768;

    myMouse.lastX = (GLfloat) (mainWindow.width / 2.0);
    myMouse.lastY = (GLfloat) (mainWindow.height / 2.0);
}

static void error_callback(int error, const char *description) {
    fprintf(stderr, "Error: %s\n", description);
}

void openGLSetup() {
    // For debug messages...
    // Set on debug flag
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(debugInfoLog, nullptr);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

    // Setup OpenGL
    // Modes for rasterization!
    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // Blending functions
    // Blending
    // ResultingColor = sourceColor * AlphaFactorSrc + destinationColor * AlphaFactorDest
    // glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    // Blend Equation: We can add src + dest, subtract, src - dest, reverse subtract, dest - src
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//        glFrontFace(GL_CW);
//    glEnable(GL_DEPTH_TEST);
//    glDepthFunc(GL_LEQUAL);

//     Particle rendering...
//    glDepthMask(GL_FALSE);
}

int main(int argc, char *argv[]) {
    // Pre-process data structures
    initDefaultWindowOptions();

    GLFWwindow *window;
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
        exit(EXIT_FAILURE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    window = glfwCreateWindow(mainWindow.width, mainWindow.height, "Particle System ", NULL, NULL);

    if (!window) {
        std::cout << "Cannot initialize window..." << std::endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    // Set the required callback functions
    glfwSetKeyCallback(window, keyCallback);

    // Later for mouse moving
    glfwSetCursorPosCallback(window, mouseCallback);
    glfwSetScrollCallback(window, scrollCallback);

    // Window resizing
    glfwSetWindowSizeCallback(window, resizeCallback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    // Camera reference save
    // Camera handler
    int windowWidth = 800;
    int windowHeight = 600;
    float aspectRatio = windowWidth / (float) windowHeight;
    PerspectiveCamera *camera = new PerspectiveCamera(glm::radians(45.0f), aspectRatio, 0.01f, 1000.f);
    glfwSetWindowUserPointer(window, camera);


    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    openGLSetup();

    /*
     * Fire assets and particle generator init
     *
     * */
    glm::vec3 initial_fire_clock_pos(1.0f, 1.0f, 1.0f);
    std::vector<CoreParticleGenerator *> fireGenerators = createFireGenerators("./assets/textures/fire_atlas.jpg", 1000,
                                                                               initial_fire_clock_pos);
    /*
     * Rain assets and particle generator init
     *
     * */
    int nrChannels;
    GLint texWidth;
    GLint texHeight;
    stbi_set_flip_vertically_on_load(true);
//    unsigned char *data = stbi_load("./assets/textures/particle.bmp", &texWidth, &texHeight, &nrChannels, 4);
    unsigned char *data = stbi_load("./assets/textures/snow.png", &texWidth, &texHeight, &nrChannels, 4);

    Texture2D *particleTex = new Texture2D{};
    particleTex->textureWrapS = GL_CLAMP_TO_EDGE;
    particleTex->textureWrapT = GL_CLAMP_TO_EDGE;
    particleTex->generate(data, texWidth, texHeight);
    stbi_image_free(data);

    glm::vec3 rain_loc(0.0f, 0.0f, 0.0f);
    ParticleGenerator particleGenerator(rain_loc, 1000, *particleTex,
                                        *(new ParticleRespawnRain()),
                                        *(new ParticleUpdaterRain()));

    SceneGraphRenderer renderer{};
    SceneNode *root = new SceneNode();
    root->addChild(&particleGenerator);
    for (auto &fireGenerator : fireGenerators) {
        root->addChild(fireGenerator);
    }

    // DeltaTime variables
    double deltaTime = 0.0f;
    double lastFrame = 0.0f;
    while (!glfwWindowShouldClose(window)) {
        // Calculate delta time
        double currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        renderer.clear();
        root->update(deltaTime);

        particleGenerator.getMaterial().setUniform("use_billboarding", billBoarding);
        for (CoreParticleGenerator *g : fireGenerators) {
            g->getMaterial().setUniform("dt", (float) deltaTime);
            g->getMaterial().setUniform("use_billboarding", billBoarding);
        }

        renderer.renderSceneGraph(root, *camera);

        glfwSwapBuffers(window);
        glfwPollEvents();
        doMovement(window, deltaTime);
    }

    glfwDestroyWindow(window);
    glfwTerminate();

    std::cout << "Finished program..." << std::endl;
//    std::cout << glGetString(GL_VERSION) << std::endl;

    exit(EXIT_SUCCESS);
    return 0;
}

std::vector<CoreParticleGenerator *>
createFireGenerators(const std::string &tex_path, int numOfParticles, glm::vec3 initialPosition) {
    int nrChannels;
    GLint texWidth;
    GLint texHeight;
    stbi_set_flip_vertically_on_load(true);
    unsigned char *data = stbi_load(tex_path.c_str(), &texWidth, &texHeight, &nrChannels, 4);

    Texture2D *particleTex = new Texture2D{};
    particleTex->textureWrapS = GL_CLAMP_TO_EDGE;
    particleTex->textureWrapT = GL_CLAMP_TO_EDGE;
    particleTex->generate(data, texWidth, texHeight);
    stbi_image_free(data);

    float radii = 3.0f;
    std::vector<CoreParticleGenerator *> generators;
    float degrees30Radians = 0.523599;
    for (int c = 0; c < 12; c++) {
        float angleRadians = c * degrees30Radians;
        float cosValue = cos(angleRadians);
        float sinValue = sin(angleRadians);
        float rotatedX = initialPosition.x * cosValue - initialPosition.y * sinValue;
        float rotatedZ = initialPosition.y * cosValue + initialPosition.x * sinValue;
        glm::vec3 offset(radii * rotatedX, 0.0f, radii * rotatedZ);

        ParticleRespawner *fireRespawn = new ParticleRespawnFire();
        ParticleUpdater *fireUpdate = new ParticleUpdaterFire();
        CoreParticleGenerator *fireGenerator = new ParticleGeneratorFire(initialPosition + offset, numOfParticles,
                                                                         *particleTex, *fireRespawn, *fireUpdate);
        generators.push_back(fireGenerator);
    }

    // Middle one
    glm::vec3 offset(0.0);

    ParticleRespawner *fireRespawn = new ParticleRespawnFire();
    ParticleUpdater *fireUpdate = new ParticleUpdaterFire();
    CoreParticleGenerator *fireGenerator = new ParticleGeneratorFire(initialPosition + offset, numOfParticles,
                                                                     *particleTex, *fireRespawn, *fireUpdate,
                                                                     glm::vec4(0.1, 1.0, 0.5, 1.0));

    generators.push_back(fireGenerator);
    return generators;
}

void resizeCallback(GLFWwindow *window, int width, int height) {
    int newWidth = (width <= 0) ? 1 : width;
    int newHeight = (height <= 0) ? 1 : height;
    glViewport(0, 0, (GLsizei) newWidth, (GLsizei) newHeight);
}

void doMovement(GLFWwindow *window, double deltaTime) {
    Camera *camera = (Camera *) glfwGetWindowUserPointer(window);
    // Camera controls
    if (keys[GLFW_KEY_W]) {
        camera->ProcessKeyboard(FORWARD, deltaTime);
    }
    if (keys[GLFW_KEY_S]) {
        camera->ProcessKeyboard(BACKWARD, deltaTime);
    }

    if (keys[GLFW_KEY_A]) {
        camera->ProcessKeyboard(LEFT, deltaTime);
    }
    if (keys[GLFW_KEY_D]) {
        camera->ProcessKeyboard(RIGHT, deltaTime);
    }

    // Camera controls
    if (keys[GLFW_KEY_UP]) {
        camera->ProcessKeyboard(LOOK_UP, deltaTime);
    }
    if (keys[GLFW_KEY_DOWN]) {
        camera->ProcessKeyboard(LOOK_DOWN, deltaTime);
    }
    if (keys[GLFW_KEY_LEFT]) {
        camera->ProcessKeyboard(LOOK_LEFT, deltaTime);
    }
    if (keys[GLFW_KEY_RIGHT]) {
        camera->ProcessKeyboard(LOOK_RIGHT, deltaTime);
    }

    if (keys[GLFW_KEY_1]) {
        billBoarding = true;
    } else if (keys[GLFW_KEY_2]) {
        billBoarding = false;
    }
}

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
    if (key >= 0 && key < 1024) {
        if (action == GLFW_PRESS) {
            keys[key] = true;
            // Key dispatcher later i.e. event dispatcher
        } else if (action == GLFW_RELEASE) {
            keys[key] = false;
        }
    }
}

// GLFW Options
//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_ENABLED);

// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
void scrollCallback(GLFWwindow *window, double xOffset, double yOffset) {
    Camera *cam = (Camera *) glfwGetWindowUserPointer(window);
    cam->ProcessMouseScroll((GLfloat) yOffset);
}

void mouseCallback(GLFWwindow *window, double xPos, double yPos) {
    if (myMouse.firstMouse) {
        myMouse.lastX = (GLfloat) xPos;
        myMouse.lastY = (GLfloat) yPos;
        myMouse.firstMouse = false;
    }

    GLfloat xOffset = (GLfloat) xPos - myMouse.lastX;
    GLfloat yOffset = myMouse.lastY - (GLfloat) yPos; // Reversed since y-coordinates go from bottom to left

    myMouse.lastX = (GLfloat) xPos;
    myMouse.lastY = (GLfloat) yPos;

    Camera *camera = (Camera *) glfwGetWindowUserPointer(window);
    camera->ProcessMouseMovement(xOffset, yOffset);
}
