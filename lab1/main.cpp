#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/norm.hpp>
#include <glm/gtx/rotate_vector.hpp>

// CPP
#include <iostream>
#include <fstream>

#include <glm/gtx/string_cast.hpp>

// OBJ Model Loader
#include <openGL/objects/loaders/OBJ_Loader.hpp>

// OpenGL Data
#include <openGL/objects/MeshObject.hpp>
#include <openGL/objects/BSpline.hpp>
#include <openGL/objects/Vertex.hpp>

#include <openGL/rendering/ShaderUtil.hpp>

#include <openGL/rendering/Shader.hpp>

// Renderer
#include <openGL/rendering/Renderer.hpp>

// Material
#include <openGL/objects/materials/Material.hpp>

// Logging
#include <general/Logger.hpp>

#include <openGL/rendering/Camera.hpp>
#include <openGL/rendering/PerspectiveCamera.hpp>
#include <openGL/objects/geometry/BufferAttribute.hpp>
#include <openGL/objects/geometry/BufferGeometry.hpp>
#include <openGL/objects/materials/BasicMeshMaterial.hpp>
#include <openGL/objects/Scene.hpp>

struct Win {
    int id;
    int width;
    int height;
};

// Structures
struct Mouse {
    int x;
    int y;
    GLfloat lastX;
    GLfloat lastY;
    bool rightPress;
    int clickIdx;
    bool firstMouse;
};

Mouse myMouse;
Win mainWindow;

// Function prototypes
void initDefaultWindowOptions();

// Callbacks
void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode);

void mouseCallback(GLFWwindow *window, double xpos, double ypos);

void scrollCallback(GLFWwindow *window, double xoffset, double yoffset);

void resizeCallback(GLFWwindow *window, int width, int height);

void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods);

void doMovement(Camera &camera, float deltaTime);

// Flags for events
bool keys[1024] = {0};

// Draw by elements or arrays
bool toggleArrays = false;

void updateObjectTransform(MeshObject &, BSpline, float, int);

void updateObjectTransform2(MeshObject &, BSpline, float, int);

glm::vec3 translateObject(BSpline, float, int);

void GLAPIENTRY debugInfoLog(GLenum source,
                             GLenum type,
                             GLuint id,
                             GLenum severity,
                             GLsizei length,
                             const GLchar *message,
                             const void *userParam) {
//    LOGN("[DEBUG MESSAGE]");
//    LOG("[SOURCE] ");
//    LOGN(source);
//    LOG("[TYPE] ");
//    LOGN(type);
//    LOG("[ID] ");
//    LOGN(id);
//    LOG("[SEVERITY] ");
//    LOGN(severity);
//    LOG("[MESSAGE] ");
//    LOGN(message);
// ignore non-significant error/warning codes
    if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

    std::cout << "---------------" << std::endl;
    std::cout << "Debug message (" << id << "): " << message << std::endl;

    switch (source) {
        case GL_DEBUG_SOURCE_API:
            std::cout << "Source: API";
            break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
            std::cout << "Source: Window System";
            break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER:
            std::cout << "Source: Shader Compiler";
            break;
        case GL_DEBUG_SOURCE_THIRD_PARTY:
            std::cout << "Source: Third Party";
            break;
        case GL_DEBUG_SOURCE_APPLICATION:
            std::cout << "Source: Application";
            break;
        case GL_DEBUG_SOURCE_OTHER:
            std::cout << "Source: Other";
            break;
    }
    std::cout << std::endl;

    switch (type) {
        case GL_DEBUG_TYPE_ERROR:
            std::cout << "Type: Error";
            break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            std::cout << "Type: Deprecated Behaviour";
            break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            std::cout << "Type: Undefined Behaviour";
            break;
        case GL_DEBUG_TYPE_PORTABILITY:
            std::cout << "Type: Portability";
            break;
        case GL_DEBUG_TYPE_PERFORMANCE:
            std::cout << "Type: Performance";
            break;
        case GL_DEBUG_TYPE_MARKER:
            std::cout << "Type: Marker";
            break;
        case GL_DEBUG_TYPE_PUSH_GROUP:
            std::cout << "Type: Push Group";
            break;
        case GL_DEBUG_TYPE_POP_GROUP:
            std::cout << "Type: Pop Group";
            break;
        case GL_DEBUG_TYPE_OTHER:
            std::cout << "Type: Other";
            break;
    }
    std::cout << std::endl;

    switch (severity) {
        case GL_DEBUG_SEVERITY_HIGH:
            std::cout << "Severity: high";
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            std::cout << "Severity: medium";
            break;
        case GL_DEBUG_SEVERITY_LOW:
            std::cout << "Severity: low";
            break;
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            std::cout << "Severity: notification";
            break;
    }
    std::cout << std::endl;
    std::cout << std::endl;

//    fprintf(stderr,
//            "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
//            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
//            type, severity, message);
}

void initDefaultWindowOptions() {
    myMouse.firstMouse = true;

    mainWindow.width = 1024;
    mainWindow.height = 800;

    myMouse.lastX = (GLfloat) (mainWindow.width / 2.0);
    myMouse.lastY = (GLfloat) (mainWindow.height / 2.0);
}

static void error_callback(int error, const char *description) {
    fprintf(stderr, "Error: %s\n", description);
}

MeshObject *getTangentMesh(BSpline &curve) {
    const float TANGENT_LENGTH = 3.0f;
    std::vector<unsigned int> indices;
    std::vector<float> positions;
    std::vector<float> normals;
    std::vector<float> texCoords;

    int indiceCounter = 0;
    for (int seg = 1; seg <= curve.getControlPoints().size() - 3; seg++) {
        for (float t = 0.0f; t < 1.0f; t += 0.1f) {
            glm::vec3 point = curve.evaluate(t, seg);
            glm::vec3 tangentVec = curve.evaluateFirstDer(t, seg);
            tangentVec = glm::normalize(tangentVec);
            tangentVec = tangentVec * TANGENT_LENGTH;
            Vertex v{point};
            Vertex v1{point + tangentVec};

            positions.push_back(v.position.x);
            positions.push_back(v.position.y);
            positions.push_back(v.position.z);
            positions.push_back(v1.position.x);
            positions.push_back(v1.position.y);
            positions.push_back(v1.position.z);

            normals.push_back(v.normal.y);
            normals.push_back(v.normal.x);
            normals.push_back(v.normal.z);
            normals.push_back(v1.normal.x);
            normals.push_back(v1.normal.y);
            normals.push_back(v1.normal.z);

            texCoords.push_back(v.texCoord.x);
            texCoords.push_back(v.texCoord.y);
            texCoords.push_back(v1.texCoord.x);
            texCoords.push_back(v1.texCoord.y);

            indices.push_back(indiceCounter++);
            indices.push_back(indiceCounter++);
        }
    }

    BufferGeometry *geom = new BufferGeometry(true);
    BufferAttribute positionAttribute = BufferAttribute(positions, 3);
    BufferAttribute normalAttribute = BufferAttribute(normals, 3);
    BufferAttribute textureAttribute = BufferAttribute(texCoords, 2);
    geom->addAttribute("vertexPosition", positionAttribute);
    geom->addAttribute("vertexNormal", normalAttribute);
    geom->addAttribute("texCoords", textureAttribute);
    geom->setIndices(indices);
    MeshObject *mesh = new MeshObject(*geom, *(new BasicMeshMaterial()));
    mesh->drawMode = GL_LINES;
    return mesh;
}


void openGLSetup() {
    // For debug messages...
    // Set on debug flag
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(debugInfoLog, nullptr);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

    // Setup OpenGL
    // Modes for rasterization!
//    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // Blending functions
    // Blending
    // ResultingColor = sourceColor * AlphaFactorSrc + destinationColor * AlphaFactorDest
    // glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    // Blend Equation: We can add src + dest, subtract, src - dest, reverse subtract, dest - src
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//        glFrontFace(GL_CW);
//    glEnable(GL_DEPTH_TEST);
//    glDepthFunc(GL_LEQUAL);

//     Particle rendering...
//    glDepthMask(GL_FALSE);
}

int main(int argc, char *argv[]) {
    // Pre-process data structures
    initDefaultWindowOptions();

    GLFWwindow *window;
    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    window = glfwCreateWindow(mainWindow.width, mainWindow.height, "BSpline Curve ", NULL, NULL);

    if (!window) {
        std::cout << "Cannot initialize window..." << std::endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    // Set the required callback functions
    glfwSetKeyCallback(window, keyCallback);

    // Later for mouse moving
    glfwSetCursorPosCallback(window, mouseCallback);

    // Mouse scroll Callback
    glfwSetScrollCallback(window, scrollCallback);

    // Window resizing
    glfwSetWindowSizeCallback(window, resizeCallback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

//    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    openGLSetup();

    ////////////////// SCENE CREATION /////////////////////

    // Setup Mesh Objects
    objl::Loader loader;
    bool loaded = loader.LoadFile("./assets/objects/cube.obj");
    BufferGeometry *geom = nullptr;
    if (!loaded) {
        std::cout << "Could not load object!" << std::endl;
        return -1;
    } else {
        geom = &BufferGeometry::fromLoader(loader);
    }

    BSpline &circularSpline = BSpline::fromFile("./assets/objects/bspline.txt");

    // Setup shader's
    std::string vertexShader = ShaderUtil::readShaderFromFile("./assets/shaders/bspline_shaders/vertex.vert");
    std::string fragmentShader = ShaderUtil::readShaderFromFile("./assets/shaders/bspline_shaders/pixel.frag");
    Shader shader{vertexShader, fragmentShader};

    Material *material = new Material(shader);
    MeshObject *mesh = new MeshObject(*geom, *material);
    mesh->setScale(glm::vec3(4.0f, 4.0f, 4.0f));
    MeshObject *tangentMesh = getTangentMesh(circularSpline);

    Scene scene{};
    scene.add(mesh);
    scene.add(tangentMesh);
    scene.add(&circularSpline);

    // Rendering setup
    Renderer renderer{};

    // Camera setup
    // Camera handler
    float aspectRatio = mainWindow.width / (float) mainWindow.height;
    PerspectiveCamera *camera = new PerspectiveCamera(glm::radians(45.0f), aspectRatio, 0.01f, 1000.0f);
    glfwSetWindowUserPointer(window, camera);

    // Animation
    float currentTime = glfwGetTime();
    float currentT = 0.0f;
    int currentSegment = 1;
    while (!glfwWindowShouldClose(window)) {
        float deltaTime = glfwGetTime() - currentTime;
        if (deltaTime > 0.01) {
            currentTime = glfwGetTime();

            if (currentT > 1.0) {
                currentT = 0.0;
                currentSegment += 1;
            } else {
                currentT += circularSpline.granularity;
            }

            if (currentSegment > (circularSpline.getControlPoints().size() - 3)) {
                currentSegment = 1;
                currentT = 0.0;
            }
        }

//         Update object which is on spline
        {

            glm::vec3 translation = translateObject(circularSpline, currentT, currentSegment);
            mesh->setPosition(translation);

            // First example
            updateObjectTransform2(*mesh, circularSpline, currentT, currentSegment);

            // Second example
//            updateObjectTransform(*mesh, circularSpline, currentT, currentSegment);
//            updateObjectTransform2(*mesh, circularSpline, 0.0, 1);
        }

        doMovement(*camera, deltaTime);
        renderer.render(scene, *camera);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);

//    std::cout << glGetString(GL_VERSION) << std::endl;

    std::cout << "Finished program..." << std::endl;
    glDeleteProgram(shader.programId);

    return 0;
}

glm::vec3 translateObject(BSpline curve, float t, int seg) {
    return curve.evaluate(t, seg);
}

void updateObjectTransform(MeshObject &mesh, BSpline curve, float t, int seg) {
    glm::vec3 w = curve.evaluateFirstDer(t, seg);
    glm::vec3 secondDerivative = curve.evaluateSecondDer(t, seg);

    glm::vec3 u;
    glm::vec3 v;
    const float EPSILON = 1E-6;
    if (glm::length2(secondDerivative) < EPSILON * EPSILON) {
        w = glm::vec3(0, 0, 1);
        u = glm::vec3(0, 1, 0);
        v = glm::vec3(1, 0, 0);
    } else {
        u = glm::cross(w, secondDerivative);
        v = glm::cross(w, u);
        w = glm::normalize(w);
        u = glm::normalize(u);
        v = glm::normalize(v);
    }

    float elementsRotational[16] = {
            w.x, u.x, v.x, 0.0,
            w.y, u.y, v.y, 0.0,
            w.z, u.z, v.z, 0.0,
            0.0, 0.0, 0.0, 1.0};

    glm::mat4 rotationalMatrix = glm::make_mat4(elementsRotational);
    rotationalMatrix = glm::inverse(rotationalMatrix);

    mesh.setTransformMatrix(glm::inverse(rotationalMatrix));
}

void updateObjectTransform2(MeshObject &mesh, BSpline curve, float t, int seg) {
    glm::vec3 s = glm::vec3(0.0f, 0.0f, 1.0f);
    glm::vec3 e = curve.evaluateFirstDer(t, seg);
    const float EPSILON = 1E-6;
    if (glm::length2(e) < EPSILON * EPSILON) {
        // Set some orientation
        e = glm::vec3(0.0f, 0.0f, 1.0f);
    }

    glm::vec3 rotateAxis = glm::cross(s, e);
    rotateAxis = glm::normalize(rotateAxis);

    float cosArg = glm::dot(s, e) / (glm::l2Norm(s) * glm::l2Norm(e));
    float angle = std::acos(cosArg);

    mesh.rotate(angle, rotateAxis);
}

void resizeCallback(GLFWwindow *window, int width, int height) {
    int newWidth = (width <= 0) ? 1 : width;
    int newHeight = (height <= 0) ? 1 : height;
    glViewport(0, 0, (GLsizei) newWidth, (GLsizei) newHeight);
}

void doMovement(Camera &camera, float deltaTime) {
    // Camera controls
    if (keys[GLFW_KEY_W]) {
        camera.ProcessKeyboard(FORWARD, deltaTime);
    }
    if (keys[GLFW_KEY_S]) {
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    }

    if (keys[GLFW_KEY_A]) {
        camera.ProcessKeyboard(LEFT, deltaTime);
    }
    if (keys[GLFW_KEY_D]) {
        camera.ProcessKeyboard(RIGHT, deltaTime);
    }

    // Camera controls
    if (keys[GLFW_KEY_UP]) {
        camera.ProcessKeyboard(LOOK_UP, deltaTime);
    }
    if (keys[GLFW_KEY_DOWN]) {
        camera.ProcessKeyboard(LOOK_DOWN, deltaTime);
    }
    if (keys[GLFW_KEY_LEFT]) {
        camera.ProcessKeyboard(LOOK_LEFT, deltaTime);
    }
    if (keys[GLFW_KEY_RIGHT]) {
        camera.ProcessKeyboard(LOOK_RIGHT, deltaTime);
    }
}

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
    if (key >= 0 && key < 1024) {
        if (action == GLFW_PRESS) {
            keys[key] = true;
            // Key dispatcher later i.e. event dispatcher
        } else if (action == GLFW_RELEASE) {
            keys[key] = false;
        }
    }
}

// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
void scrollCallback(GLFWwindow *window, double xOffset, double yOffset) {
    Camera *camera = (Camera *) glfwGetWindowUserPointer(window);
    camera->ProcessMouseScroll((GLfloat) yOffset);
}

void mouseCallback(GLFWwindow *window, double xPos, double yPos) {
    Camera *camera = (Camera *) glfwGetWindowUserPointer(window);
    if (myMouse.firstMouse) {
        myMouse.lastX = (GLfloat) xPos;
        myMouse.lastY = (GLfloat) yPos;
        myMouse.firstMouse = false;
    }

    GLfloat xOffset = (GLfloat) xPos - myMouse.lastX;
    GLfloat yOffset = myMouse.lastY - (GLfloat) yPos; // Reversed since y-coordinates go from bottom to left

    myMouse.lastX = (GLfloat) xPos;
    myMouse.lastY = (GLfloat) yPos;

    camera->ProcessMouseMovement(xOffset, yOffset);
}
