#include <openGL/client/level-editor/SceneManager.hpp>
#include <openGL/rendering/Renderer.hpp>
#include <openGL/objects/Scene.hpp>
#include <openGL/rendering/PerspectiveCamera.hpp>
#include <iostream>
#include <GLFW/glfw3.h>
#include <openGL/client/state-design/IdleEventState.hpp>
#include <openGL/client/state-design/AddLineState.hpp>

EventState *IDLE_EVENT_STATE = nullptr;

SceneManager::SceneManager(Renderer &renderer, Scene &scene) : keys(), camera(buildCamera()), renderer(renderer),
                                                               scene(scene) {
    IDLE_EVENT_STATE = new IdleEventState();
    state = IDLE_EVENT_STATE;
}

void SceneManager::update(double deltaTime) {

    // For collision and other updates (animations)
    // const delta = this.clock.getDelta();
    //for each sceneSubject.update(delta);

    // For Camera movement, animation...
    //this.cameraControl.update(delta);
    doMovement(deltaTime);
    updateState(deltaTime);
    renderer.render(scene, camera);
    state->afterDraw(renderer, camera);
}

void SceneManager::mouseMove(double x, double y) {
    state->onMouseMove(x, y);
}

void SceneManager::onMouseMove(float xOffset, float yOffset) {
    camera.ProcessMouseMovement(xOffset, yOffset);
}

void SceneManager::onWindowResize(int newWidth, int newHeight) {
    camera.setAspectRatio((float) newWidth / newHeight);
}

PerspectiveCamera SceneManager::buildCamera() {
    // Camera handler
    int windowWidth = 1024;
    int windowHeight = 800;
    float aspectRatio = windowWidth / (float) windowHeight;
    return PerspectiveCamera(glm::radians(45.0f), aspectRatio, 0.01f, 1000.f);
}

void SceneManager::onKeyRelease(int key, int mods) {
//    std::cout << "Key released: " << key << std::endl;
//    std::cout << "Shift was on: " << (mods == GLFW_MOD_SHIFT) << std::endl;
    keys[key] = false;
}

void SceneManager::onKeyPress(int key, int mods) {
//    std::cout << "Key pressed: " << key << std::endl;
//    std::cout << "Shift was on: " << (mods == GLFW_MOD_SHIFT) << std::endl;
    keys[key] = true;
}

void SceneManager::doMovement(float deltaTime) {
    if (keys[GLFW_KEY_W]) {
        camera.ProcessKeyboard(FORWARD, deltaTime);
    }
    if (keys[GLFW_KEY_S]) {
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    }
    if (keys[GLFW_KEY_A]) {
        camera.ProcessKeyboard(LEFT, deltaTime);
    }
    if (keys[GLFW_KEY_D]) {
        camera.ProcessKeyboard(RIGHT, deltaTime);
    }

    // Camera controls
    if (keys[GLFW_KEY_UP]) {
        camera.ProcessKeyboard(LOOK_UP, deltaTime);
    }
    if (keys[GLFW_KEY_DOWN]) {
        camera.ProcessKeyboard(LOOK_DOWN, deltaTime);
    }
    if (keys[GLFW_KEY_LEFT]) {
        camera.ProcessKeyboard(LOOK_LEFT, deltaTime);
    }
    if (keys[GLFW_KEY_RIGHT]) {
        camera.ProcessKeyboard(LOOK_RIGHT, deltaTime);
    }


}

void SceneManager::onMouseScroll(float yOffset) {
    camera.ProcessMouseScroll((GLfloat) yOffset);
}

void SceneManager::updateState(double time) {
    if (keys[GLFW_KEY_L]) {
        // Set state to draw line.
        state->onLeaving();
        state = new AddLineState(this->scene);
    }

    if (keys[GLFW_KEY_ESCAPE]) {
        state->onLeaving();
        state = IDLE_EVENT_STATE;
    }
}

void SceneManager::onMouseClick(glm::vec2 mousePosition, int button, int action, int mods) {
    state->onMouseClick(mousePosition, button, action, mods);

    if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        state->onLeaving();
        state = IDLE_EVENT_STATE;
    }
}

const Camera &SceneManager::getCamera() {
    return camera;
}
