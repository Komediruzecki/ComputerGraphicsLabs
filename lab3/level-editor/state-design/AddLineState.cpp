#include <openGL/client/state-design/AddLineState.hpp>
#include <openGL/objects/Scene.hpp>
#include <openGL/objects/Object3D.hpp>
#include <glm/vec2.hpp>
#include <openGL/objects/geometry/LineGeometry.hpp>
#include <openGL/objects/materials/Material.hpp>
#include <openGL/objects/MeshObject.hpp>
#include <openGL/rendering/Renderer.hpp>
#include <GLFW/glfw3.h>

//AddLineState::AddLineState(Scene &scene, const Object3D &prototype) : scene(scene), prototype(prototype) {
//
//}

AddLineState::AddLineState(Scene &scene) : scene(scene) {

}

void AddLineState::onMouseMove(double x, double y) {
    secondPoint = glm::vec2(x, y);
}


void AddLineState::onMouseClick(glm::vec2 mousePos, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        if (firstClick) {
            firstClick = false;

            firstPoint = glm::vec2(mousePos);
        } else {
            secondPoint = glm::vec2(mousePos);

            LineGeometry *geometry = new LineGeometry(firstPoint, secondPoint);
            Material *material = new Material();
            MeshObject *obj = new MeshObject(*geometry, *material);
            scene.add(obj);

            // reset fields
//            reset();
        }
    }
}

void AddLineState::reset() {
//    firstPoint = nullptr;
//    secondPoint = nullptr;
//    firstClick = false;
}

void AddLineState::afterDraw(Renderer &r, const Camera &camera) {
    LineGeometry *geometry = new LineGeometry(firstPoint, secondPoint);
    MeshObject *obj = new MeshObject(*geometry, *(new Material()));
    r.renderMesh(*obj, camera);
}
