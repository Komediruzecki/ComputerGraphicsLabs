#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <openGL/EngineCore.hpp>

#include <stdlib.h>
#include <glm/glm.hpp>

#include <glm/gtc/matrix_transform.hpp>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

#include <glm/gtx/rotate_vector.hpp>
// CPP
#include <iostream>

#include <fstream>
#include <openGL/rendering/ShaderUtil.hpp>
#include <openGL/rendering/Shader.hpp>

// OBJ Model Loader
#include <openGL/objects/loaders/OBJ_Loader.hpp>

// OpenGL Data
#include <openGL/objects/MeshObject.hpp>
#include <openGL/objects/BSpline.hpp>
#include <openGL/objects/scene-graph/SceneNode.hpp>
#include <openGL/objects/scene-graph/CubeRobot.hpp>
#include <openGL/rendering/SceneGraphRenderer.hpp>
#include <openGL/rendering/ShaderUtil.hpp>

#include <openGL/rendering/ShaderUtil.hpp>

// Renderer
#include <openGL/rendering/Renderer.hpp>

// Material

// Logging
#include <general/Logger.hpp>

#include <openGL/objects/ParticleGenerator.hpp>
#include <openGL/objects/materials/Material.hpp>
#include <openGL/objects/geometry/BufferGeometry.hpp>
#include <openGL/objects/Scene.hpp>
#include <openGL/math/matrix/MatrixUtil.hpp>
#include "openGL/client/state-design/EventManager.hpp"
#include "openGL/client/state-design/IdleEventState.hpp"
#include "openGL/client/level-editor/SceneManager.hpp"

// Image
#include<image_load/stb_image.h>
#include <openGL/rendering/particle_system/ParticleUpdaterFire.hpp>
#include "openGL/rendering/particle_system/ParticleGeneratorFire.hpp"
#include "openGL/rendering/particle_system/ParticleRespawnFire.hpp"

struct Win {
    int id;
    int width;
    int height;
};

// Structures
struct Mouse {
    int x;
    int y;
    GLfloat lastX;
    GLfloat lastY;
    bool rightPress;
    int clickIdx;
    bool firstMouse;
};

Mouse myMouse;
Win mainWindow;

// Event Managing with state design pattern
IdleEventState *idleState = new IdleEventState();
EventManager *eventManager = new EventManager(*idleState);

// Function prototypes
void initDefaultWindowOptions();

// Callbacks
void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    SceneManager *manager = (SceneManager *) glfwGetWindowUserPointer(window);

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }

    if (key >= 0 && key < 1024) {
        if (action == GLFW_PRESS) {
            manager->onKeyPress(key, mods);
        } else if (action == GLFW_RELEASE) {
            manager->onKeyRelease(key, mods);
        }
    }
}

void scrollCallback(GLFWwindow *window, double xOffset, double yOffset) {
    SceneManager *manager = (SceneManager *) glfwGetWindowUserPointer(window);
    manager->onMouseScroll(yOffset);
}

void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods) {
    double xPos, yPos;
    glfwGetCursorPos(window, &xPos, &yPos);

    glm::vec2 mousePos{xPos, yPos};
    // todo: Add multiple states i.e. events; layers; event propagation;
    eventManager->currentState.onMouseClick(mousePos, button, action, mods);
    SceneManager *manager = (SceneManager *) glfwGetWindowUserPointer(window);
    manager->onMouseClick(mousePos, button, action, mods);
}

void mouseCallback(GLFWwindow *window, double xpos, double ypos);

void resizeCallback(GLFWwindow *window, int width, int height);


void GLAPIENTRY debugInfoLog(GLenum source,
                             GLenum type,
                             GLuint id,
                             GLenum severity,
                             GLsizei length,
                             const GLchar *message,
                             const void *userParam) {
//    LOGN("[DEBUG MESSAGE]");
//    LOG("[SOURCE] ");
//    LOGN(source);
//    LOG("[TYPE] ");
//    LOGN(type);
//    LOG("[ID] ");
//    LOGN(id);
//    LOG("[SEVERITY] ");
//    LOGN(severity);
//    LOG("[MESSAGE] ");
//    LOGN(message);
// ignore non-significant error/warning codes
    if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

    std::cout << "---------------" << std::endl;
    std::cout << "Debug message (" << id << "): " << message << std::endl;

    switch (source) {
        case GL_DEBUG_SOURCE_API:
            std::cout << "Source: API";
            break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
            std::cout << "Source: Window System";
            break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER:
            std::cout << "Source: Shader Compiler";
            break;
        case GL_DEBUG_SOURCE_THIRD_PARTY:
            std::cout << "Source: Third Party";
            break;
        case GL_DEBUG_SOURCE_APPLICATION:
            std::cout << "Source: Application";
            break;
        case GL_DEBUG_SOURCE_OTHER:
            std::cout << "Source: Other";
            break;
    }
    std::cout << std::endl;

    switch (type) {
        case GL_DEBUG_TYPE_ERROR:
            std::cout << "Type: Error";
            break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            std::cout << "Type: Deprecated Behaviour";
            break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            std::cout << "Type: Undefined Behaviour";
            break;
        case GL_DEBUG_TYPE_PORTABILITY:
            std::cout << "Type: Portability";
            break;
        case GL_DEBUG_TYPE_PERFORMANCE:
            std::cout << "Type: Performance";
            break;
        case GL_DEBUG_TYPE_MARKER:
            std::cout << "Type: Marker";
            break;
        case GL_DEBUG_TYPE_PUSH_GROUP:
            std::cout << "Type: Push Group";
            break;
        case GL_DEBUG_TYPE_POP_GROUP:
            std::cout << "Type: Pop Group";
            break;
        case GL_DEBUG_TYPE_OTHER:
            std::cout << "Type: Other";
            break;
    }
    std::cout << std::endl;

    switch (severity) {
        case GL_DEBUG_SEVERITY_HIGH:
            std::cout << "Severity: high";
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            std::cout << "Severity: medium";
            break;
        case GL_DEBUG_SEVERITY_LOW:
            std::cout << "Severity: low";
            break;
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            std::cout << "Severity: notification";
            break;
    }
    std::cout << std::endl;
    std::cout << std::endl;

//    fprintf(stderr,
//            "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
//            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
//            type, severity, message);
}


void initDefaultWindowOptions() {
    myMouse.firstMouse = true;

    mainWindow.width = 1024;
    mainWindow.height = 768;

    myMouse.lastX = (GLfloat) (mainWindow.width / 2.0);
    myMouse.lastY = (GLfloat) (mainWindow.height / 2.0);
}

static void error_callback(int error, const char *description) {
    fprintf(stderr, "Error: %s\n", description);
}

void openGLSetup() {
    // For debug messages...
    // Set on debug flag
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(debugInfoLog, nullptr);

    // Setup OpenGL
    // Modes for rasterization!
    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // Blending functions
    // Blending
    // ResultingColor = sourceColor * AlphaFactorSrc + destinationColor * AlphaFactorDest
    // glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    // Blend Equation: We can add src + dest, subtract, src - dest, reverse subtract, dest - src
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//        glFrontFace(GL_CW);
//    glEnable(GL_DEPTH_TEST);
//    glDepthFunc(GL_LEQUAL);

//     Particle rendering...
//    glDepthMask(GL_FALSE);
}

std::vector<CoreParticleGenerator *>
createFireGenerators(const std::string &tex_path, int numOfParticles, glm::vec3 initialPosition) {
    int nrChannels;
    GLint texWidth;
    GLint texHeight;
    stbi_set_flip_vertically_on_load(true);
    unsigned char *data = stbi_load(tex_path.c_str(), &texWidth, &texHeight, &nrChannels, 4);

    Texture2D *particleTex = new Texture2D{};
    particleTex->textureWrapS = GL_CLAMP_TO_EDGE;
    particleTex->textureWrapT = GL_CLAMP_TO_EDGE;
    particleTex->generate(data, texWidth, texHeight);
    stbi_image_free(data);

    float radii = 3.0f;
    std::vector<CoreParticleGenerator *> generators;
    float degrees30Radians = 0.523599;
    for (int c = 0; c < 12; c++) {
        float angleRadians = c * degrees30Radians;
        float cosValue = cos(angleRadians);
        float sinValue = sin(angleRadians);
        float rotatedX = initialPosition.x * cosValue - initialPosition.y * sinValue;
        float rotatedZ = initialPosition.y * cosValue + initialPosition.x * sinValue;
        glm::vec3 offset(radii * rotatedX, 0.0f, radii * rotatedZ);

        ParticleRespawner *fireRespawn = new ParticleRespawnFire();
        ParticleUpdater *fireUpdate = new ParticleUpdaterFire();
        CoreParticleGenerator *fireGenerator = new ParticleGeneratorFire(initialPosition + offset, numOfParticles,
                                                                         *particleTex, *fireRespawn, *fireUpdate);
        generators.push_back(fireGenerator);
    }

    // Middle one
    glm::vec3 offset(0.0);

    ParticleRespawner *fireRespawn = new ParticleRespawnFire();
    ParticleUpdater *fireUpdate = new ParticleUpdaterFire();
    CoreParticleGenerator *fireGenerator = new ParticleGeneratorFire(initialPosition + offset, numOfParticles,
                                                                     *particleTex, *fireRespawn, *fireUpdate,
                                                                     glm::vec4(0.1, 1.0, 0.5, 1.0));

    generators.push_back(fireGenerator);
    return generators;
}

int main(int argc, char *argv[]) {
    // Pre-process data structures
    initDefaultWindowOptions();

    GLFWwindow *window;
    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    window = glfwCreateWindow(mainWindow.width, mainWindow.height, "GameEditor System ", NULL, NULL);

    if (!window) {
        std::cout << "Cannot initialize window..." << std::endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    // Set the required callback functions
    glfwSetKeyCallback(window, keyCallback);

    // Later for mouse moving
    glfwSetCursorPosCallback(window, mouseCallback);

    // Mouse Button Callback
    glfwSetMouseButtonCallback(window, mouseButtonCallback);

    // Mouse scroll Callback
    glfwSetScrollCallback(window, scrollCallback);

    // Window resizing
    glfwSetWindowSizeCallback(window, resizeCallback);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);

    // Other options and render loop
//    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    openGLSetup();


    ////////////////// SCENE CREATION /////////////////////
    Material &material = ShaderUtil::createMaterialFromShaders("./assets/shaders/basic_mesh_shader/vertex.vert",
                                                               "./assets/shaders/basic_mesh_shader/pixel.frag");
    // Set uniforms
    material.setUniform("meshColor", glm::vec3(1.0f, 0.0f, 0.0f));

    // Materials
    Material &material2 = material.copy();
    // Set uniforms
    material2.setUniform("meshColor", glm::vec3(0.0f, 1.0f, 0.0f));

    // Rendering setup
    Renderer renderer{};

    ////////////////// Scene Setup ///////////////////////
    // Setup Mesh Objects
    BufferGeometry *cubeGeometry = nullptr;
    BufferGeometry *f16Geometry = nullptr;

    objl::Loader loader;
    bool loaded = loader.LoadFile("./assets/objects/cube.obj");
    if (!loaded) {
        std::cout << "Could not load object!" << std::endl;
        return -1;
    } else {
        cubeGeometry = &BufferGeometry::fromLoader(loader);
    }

    loaded = loader.LoadFile("./assets/objects/f16.obj");
    if (!loaded) {
        std::cout << "Could not load object!" << std::endl;
        return -1;
    } else {
        f16Geometry = &BufferGeometry::fromLoader(loader);
    }

    MeshObject *cubeObject = new MeshObject(*f16Geometry, material);
    MeshObject *cubeObject2 = new MeshObject(*f16Geometry, material2);

    Scene scene{};
    scene.add(cubeObject);
    scene.add(cubeObject2);
    ////////////////// Scene Setup ///////////////////////

//    // Init scene manager
    SceneManager *sceneManager = new SceneManager(renderer, scene);
    glfwSetWindowUserPointer(window, sceneManager);

    /*
       * Rain assets and particle generator init
       *
       * */
    int nrChannels;
    GLint texWidth;
    GLint texHeight;
    stbi_set_flip_vertically_on_load(true);
    unsigned char *data = stbi_load("./assets/textures/snow.png", &texWidth, &texHeight, &nrChannels, 4);

    Texture2D *particleTex = new Texture2D{};
    particleTex->textureWrapS = GL_CLAMP_TO_EDGE;
    particleTex->textureWrapT = GL_CLAMP_TO_EDGE;
    particleTex->generate(data, texWidth, texHeight);
    stbi_image_free(data);
    glm::vec3 rain_loc(0.0f, 10.0f, 5.0f);
    ParticleGenerator particleGenerator(rain_loc, 1000, *particleTex,
                                        *(new ParticleRespawnRain()),
                                        *(new ParticleUpdaterRain()));
    //////////////////// GRAPH SCENE SETUP
    SceneGraphRenderer sceneGraphRenderer{};
    CubeRobot::createCube();
    CubeRobot *cubeRobot = new CubeRobot();
    SceneNode *root = new SceneNode();
    root->addChild(cubeRobot);
    root->addChild(&particleGenerator);

    glm::vec3 initial_fire_clock_pos(1.0f, 1.0f, 1.0f);
    std::vector<CoreParticleGenerator *> fireGenerators = createFireGenerators("./assets/textures/fire_atlas.jpg", 1000,
                                                                               initial_fire_clock_pos);
    for (auto &fireGenerator : fireGenerators) {
        root->addChild(fireGenerator);
    }
    //////////////////// GRAPH SCENE SETUP


    // DeltaTime variables
    double lastFrame = 0.0f;
    double animTime = 0.0f;

    struct animState {
        float rotAngleA;
        int upperBoundA;
        float rotAngleB;
        int upperBoundB;
        int rotBDir;
        float rotAngleC;
        int upperBoundC;
        glm::vec3 axisA;
        glm::vec3 axisB;
        glm::vec3 axisC;
        int animSpeed;
    } animationState = {0.0f, 90, 0.0f, 60, 1, 0.0f, 60, glm::vec3(0, 1, 0), glm::vec3(1, 0, 0), glm::vec3(0, 0, 1), 1};


    float clockTime = 3.0f;
    while (!glfwWindowShouldClose(window)) {
        // Calculate delta time
        double currentFrame = glfwGetTime();
        double deltaTime = currentFrame - lastFrame;
        clockTime += deltaTime;

        animTime += deltaTime;
        if (animTime > 0.03) {
            if (animationState.rotAngleA < animationState.upperBoundA) {
                cubeObject->rotate(animationState.animSpeed, animationState.axisA);
                animationState.rotAngleA += animationState.animSpeed;
            } else if (animationState.rotAngleB < animationState.upperBoundB) {
                cubeObject->rotate(animationState.rotBDir * animationState.animSpeed, animationState.axisB);
                animationState.rotAngleB += animationState.animSpeed;
            } else if (animationState.rotBDir == 1) {
                animationState.rotAngleB = 0;
                animationState.rotBDir *= -1;
            } else if (animationState.rotAngleC < animationState.upperBoundC) {
                cubeObject->rotate(animationState.animSpeed, animationState.axisC);
                animationState.rotAngleC += animationState.animSpeed;
            } else {
                cubeObject->setRotation(Quaternion(1, 0, 0, 0));
                animationState.rotAngleA = 0;
                animationState.rotAngleB = 0;
                animationState.rotAngleC = 0;
                animationState.rotBDir = 1;
            }

            float bAngle = animationState.rotBDir == -1 ? animationState.upperBoundB - animationState.rotAngleB
                                                        : animationState.rotAngleB;
            glm::mat4 ctm{1.0};
            ctm = glm::translate(ctm, glm::vec3(0, 0, 5));
            glm::mat4 rotY = MatrixUtil::rotateY(animationState.rotAngleA);
            glm::mat4 rotX = MatrixUtil::rotateX(bAngle);
            glm::mat4 rotZ = MatrixUtil::rotateZ(animationState.rotAngleC);
            cubeObject2->setTransformMatrix(ctm * rotZ * rotY * rotX);

            animTime = 0.0;
        }

        particleGenerator.getMaterial().setUniform("use_billboarding", true);
        for (CoreParticleGenerator *g : fireGenerators) {
            g->getMaterial().setUniform("dt", clockTime);
            g->getMaterial().setUniform("use_billboarding", true);
        }


        lastFrame = currentFrame;
        sceneManager->update(deltaTime);
        root->update(deltaTime);

        // Draw after update!
        sceneGraphRenderer.renderSceneGraph(root, sceneManager->getCamera());

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);
    glfwTerminate();

    std::cout << "Game Editor Stopped..." << std::endl;

    // Release resources
    CubeRobot::deleteCube();
    exit(EXIT_SUCCESS);
    return 0;
}

void resizeCallback(GLFWwindow *window, int width, int height) {
    int newWidth = (width <= 0) ? 1 : width;
    int newHeight = (height <= 0) ? 1 : height;
    glViewport(0, 0, (GLsizei) newWidth, (GLsizei) newHeight);

    SceneManager *manager = (SceneManager *) glfwGetWindowUserPointer(window);
    manager->onWindowResize(newWidth, newHeight);
}

void mouseCallback(GLFWwindow *window, double xPos, double yPos) {
    if (myMouse.firstMouse) {
        myMouse.lastX = (GLfloat) xPos;
        myMouse.lastY = (GLfloat) yPos;
        myMouse.firstMouse = false;
    }

    GLfloat xOffset = (GLfloat) xPos - myMouse.lastX;
    GLfloat yOffset = myMouse.lastY - (GLfloat) yPos; // Reversed since y-coordinates go from bottom to left

    myMouse.lastX = (GLfloat) xPos;
    myMouse.lastY = (GLfloat) yPos;

    SceneManager *manager = (SceneManager *) glfwGetWindowUserPointer(window);
    manager->onMouseMove(xOffset, yOffset);
    manager->mouseMove(xPos, yPos);
}
