#version 330 core

layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec3 vertexNormal;
layout (location = 2) in vec2 texCoords;

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;

uniform vec3 meshColor;

out vec3 color;

void main() {
    gl_Position =  projectionMatrix * modelViewMatrix * vec4(vertexPosition, 1.0);
    color = meshColor;
}
