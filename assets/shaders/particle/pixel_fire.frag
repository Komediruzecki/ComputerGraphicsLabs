#version 330 core

uniform float dt;
uniform vec4 fire_color;

in float lifetime;
in vec2 tex_coords;

uniform sampler2D fire_atlas;

out vec4 frag_color;
void main (void) {
  float time = mod(dt, lifetime);
  float percent_of_life = time / lifetime;
  percent_of_life = clamp(percent_of_life, 0.0, 1.0);

  float offset = floor(16.0 * percent_of_life);
  float offset_x = floor(mod(offset, 4.0)) / 4.0;
  float offset_y = 0.75 - floor(offset / 4.0) / 4.0;

  vec4 tex_color= texture2D(
    fire_atlas,
    vec2(
      (tex_coords.x / 4.0) + offset_x,
      (tex_coords.y / 4.0) + offset_y
  ));

  frag_color = fire_color * tex_color;
  frag_color.a *= lifetime;
}