#version 330 core
layout (location = 0) in vec2 vertexPosition;
layout (location = 1) in vec2 vertexTexCoord;

out vec2 tex_coords;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;

uniform vec3 generator_location;
uniform vec3 offset;
uniform float particle_size;

uniform bool use_billboarding;

void main(){
    tex_coords = vertexTexCoord;

    vec3 cam_right = vec3(viewMatrix[0].x, viewMatrix[1].x, viewMatrix[2].x);
    vec3 cam_up = vec3(viewMatrix[0].y, viewMatrix[1].y, viewMatrix[2].y);
    vec3 particle_center = generator_location + offset;

    vec4 position = vec4(particle_center, 1.0);
    if(use_billboarding){
        position.xyz +=
            + cam_right * vertexPosition.x * particle_size
            + cam_up * vertexPosition.y * particle_size;
    } else {
        position.xy += vertexPosition.xy * particle_size;
    }

     gl_Position = (projectionMatrix * viewMatrix) * position;
}