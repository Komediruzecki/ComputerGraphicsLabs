#version 330 core
in vec2 tex_coords;

uniform vec4 particle_color;
uniform sampler2D sprite;

out vec4 fragColor;
void main()
{
    vec4 tex_color = texture(sprite, tex_coords);
    fragColor = tex_color * particle_color;
}