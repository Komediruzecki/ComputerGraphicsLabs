#version 330 core
layout (location = 0) in float particleLifetime;
layout (location = 1) in vec2 vertexTexCoord;
layout (location = 2) in vec2 vertexPosition;
layout (location = 3) in vec3 particleOffset;
layout (location = 3) in vec3 particleVelocity;

out vec2 tex_coords;
out float lifetime;

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 viewMatrix;

uniform vec3 fire_origin;

uniform bool use_billboarding;
uniform float dt;

void main(){
    float time = mod(dt, particleLifetime);

    vec3 cam_right = vec3(viewMatrix[0].x, viewMatrix[1].x, viewMatrix[2].x);
    vec3 cam_up = vec3(viewMatrix[0].y, viewMatrix[1].y, viewMatrix[2].y);
    vec3 particle_center = fire_origin + particleOffset + (time * particleVelocity);

    vec4 position = vec4(particle_center, 1.0);

    lifetime = 1.3 - (time / particleLifetime);
    lifetime = clamp(lifetime, 0.0, 1.0);
    float particle_size =  (lifetime * lifetime) * 0.05;

    if(use_billboarding) {
        position.xyz +=
            + cam_right * vertexPosition.x * particle_size
            + cam_up * vertexPosition.y * particle_size;
    } else {
        position.xy += vertexPosition.xy * particle_size;
    }

     gl_Position = projectionMatrix * viewMatrix * position;

     // Output to fragment
     tex_coords = vertexTexCoord;
     lifetime = particleLifetime;
}