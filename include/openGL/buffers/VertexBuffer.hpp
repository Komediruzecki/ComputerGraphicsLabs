#ifndef VERTEX_BUFFER_H
#define VERTEX_BUFFER_H

class VertexBuffer {
private:
    unsigned int rendererID;

public:
    // Size in bytes, count is number of items
    VertexBuffer(const void *data, unsigned int size);

    ~VertexBuffer();

    void bind() const;

    void unbind() const;
};

#endif /* VERTEX_BUFFER_H */