#ifndef INDEX_BUFFER_H
#define INDEX_BUFFER_H

class IndexBuffer {
private:
    unsigned int rendererID;
    unsigned int count;

public:
    // Size in bytes, count is number of items
    IndexBuffer(const unsigned int *data, unsigned int count);

    ~IndexBuffer();

    void bind() const;

    void unbind() const;

    unsigned int getCount() const;
};

#endif /* INDEX_BUFFER_H */