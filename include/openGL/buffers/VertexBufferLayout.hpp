#ifndef VERTEX_BUFFER_LAYOUT_H
#define VERTEX_BUFFER_LAYOUT_H

#include <vector>
#include <glad/glad.h>
#include<iostream>

struct VertexBufferElement {
    unsigned int type;
    unsigned int count;
    unsigned char normalized;

    static unsigned int getSizeOfType(unsigned int type) {
        switch (type) {
            case GL_FLOAT:
                return 4;
            case GL_UNSIGNED_INT:
                return 4;
            case GL_UNSIGNED_BYTE:
                return 1;
            case GL_INT:
                return 4;
            default:
                std::cerr << "No such type implemented for vertex buffer element " << std::to_string(type) << std::endl;
                throw std::invalid_argument("No such type implemented!");
        }
    }
};

// todo: could do in .cpp also, to avoid inlines etc...
class VertexBufferLayout {
private:
    std::vector<VertexBufferElement> elements;
    unsigned int stride;

public:
    VertexBufferLayout() : stride(0) {}

    // todo: should this be inline?
    template<typename T>
    void push(unsigned int count) {
        static_assert(true);
    }

    inline const std::vector<VertexBufferElement> &getElements() const {
        return elements;
    }

    inline unsigned int getStride() const { return stride; }
};

template<>
inline void VertexBufferLayout::push<float>(unsigned int count) {
    elements.push_back({GL_FLOAT, count, GL_FALSE});
    stride += count * VertexBufferElement::getSizeOfType(GL_FLOAT);
}

template<>
inline void VertexBufferLayout::push<unsigned int>(unsigned int count) {
    elements.push_back({GL_UNSIGNED_INT, count, GL_FALSE});
    stride += count * VertexBufferElement::getSizeOfType(GL_UNSIGNED_INT);
}


template<>
inline void VertexBufferLayout::push<int>(unsigned int count) {
    elements.push_back({GL_INT, count, GL_FALSE});
    stride += count * VertexBufferElement::getSizeOfType(GL_INT);
}

template<>
inline void VertexBufferLayout::push<unsigned char>(unsigned int count) {
    elements.push_back({GL_UNSIGNED_BYTE, count, GL_TRUE});
    stride += count * VertexBufferElement::getSizeOfType(GL_UNSIGNED_BYTE);
}

#endif /* VERTEX_BUFFER_LAYOUT_H */