#ifndef VERTEX_ARRAY_H
#define VERTEX_ARRAY_H

class VertexBuffer;

class VertexBufferLayout;
//#include <openGL/buffers/VertexBuffer.hpp>
//#include <openGL/buffers/VertexBufferLayout.hpp>

class VertexArray {

private:
    unsigned int rendererID;

public:
    VertexArray();

    ~VertexArray();

    void bind() const;

    void unbind() const;

    void addBuffer(const VertexBuffer &vb, const VertexBufferLayout &layout);
};

#endif /* VERTEX_ARRAY_H */