#ifndef GAMEENGINE_ENGINECORE_HPP
#define GAMEENGINE_ENGINECORE_HPP

#define GLM_ENABLE_EXPERIMENTAL

#include<iostream>
#include<glm/glm.hpp>
#include<glm/gtx/string_cast.hpp>
#include<string>

inline std::string matrixAsString(glm::mat4 m) {
    return glm::to_string(m);
}

inline void printMatrix(glm::mat4 m) {
    std::cout << glm::to_string(m) << std::endl;
}

#endif //GAMEENGINE_ENGINECORE_HPP
