#ifndef GAME_ENGINE_MATRIXUTIL_HPP
#define GAME_ENGINE_MATRIXUTIL_HPP

#include <glm/detail/type_mat.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext.hpp>

class MatrixUtil {
public:
    inline static glm::mat4 translation(float x, float y, float z) {
        return glm::translate(glm::mat4(1.0f), glm::vec3(x, y, z));
    }

    inline static glm::mat4 translation(glm::vec3 translations) {
        return glm::translate(glm::mat4(1.0f), translations);
    }

    inline static glm::mat4 scale(float x, float y, float z) {
        return glm::scale(glm::mat4(1.0f), glm::vec3(x, y, z));
    }

    inline static glm::mat4 scale(glm::vec3 scalers) {
        return glm::scale(glm::mat4(1.0f), scalers);
    }

    inline static glm::mat4 rotateX(float angleDegrees) {
        float cosAngle = static_cast<float>(cos(glm::radians(angleDegrees)));
        float sinAngle = static_cast<float>(sin(glm::radians(angleDegrees)));
        float elements[16] = {
                1, 0, 0, 0,
                0, cosAngle, -sinAngle, 0,
                0, sinAngle, cosAngle, 0,
                0, 0, 0, 1
        };
        glm::mat4 rotMatrix = glm::make_mat4(elements);
        return glm::transpose(rotMatrix);
    }

    inline static glm::mat4 rotateY(float angleDegrees) {
        float cosAngle = static_cast<float>(cos(glm::radians(angleDegrees)));
        float sinAngle = static_cast<float>(sin(glm::radians(angleDegrees)));
        float elements[16] = {
                cosAngle, 0, sinAngle, 0,
                0, 1, 0, 0,
                -sinAngle, 0, cosAngle, 0,
                0, 0, 0, 1
        };
        glm::mat4 rotMatrix = glm::make_mat4(elements);
        return glm::transpose(rotMatrix);
    }

    inline static glm::mat4 rotateZ(float angleDegrees) {
        float cosAngle = static_cast<float>(cos(glm::radians(angleDegrees)));
        float sinAngle = static_cast<float>(sin(glm::radians(angleDegrees)));
        float elements[16] = {
                cosAngle, -sinAngle, 0, 0,
                sinAngle, cosAngle, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        };
        glm::mat4 rotMatrix = glm::make_mat4(elements);
        return glm::transpose(rotMatrix);
    }
};

#endif //GAME_ENGINE_MATRIXUTIL_HPP
