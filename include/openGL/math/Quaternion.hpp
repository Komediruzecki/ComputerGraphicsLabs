#ifndef GAME_ENGINE_QUATERNION_HPP
#define GAME_ENGINE_QUATERNION_HPP

#include<iostream>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/detail/type_mat.hpp>

class Quaternion {
private:
    float s;
    glm::vec3 v;
public:
    // Creation
    Quaternion(float s, float x, float y, float z);

    Quaternion(glm::vec4 elements);

    ~Quaternion();

    Quaternion(float s, glm::vec3 elements);

    // Operators
    friend Quaternion &operator+(const Quaternion &lhs, const Quaternion &rhs);

    friend Quaternion &operator-(const Quaternion &lhs, const Quaternion &rhs);

    friend Quaternion &operator*(const Quaternion &lhs, const Quaternion &rhs);

    friend Quaternion &operator*(const Quaternion &lhs, double scalar);

    friend Quaternion &operator*(double scalar, const Quaternion &lhs);

    friend std::ostream &operator<<(std::ostream &stream, const Quaternion &q);

    Quaternion &operator+=(const Quaternion &rhs);

    Quaternion &operator-=(const Quaternion &rhs);

    Quaternion &operator+=(Quaternion &rhs);

    Quaternion &operator-=(Quaternion &rhs);

    Quaternion &operator=(const Quaternion &other);

    // Operations
    Quaternion &add(Quaternion q);

    Quaternion &sub(Quaternion q);

    Quaternion &mul(Quaternion q);

    /**
     * Adds rotation to this quaternion.
     * @param point point of rotation
     * @param angle angle in degrees
     * @param axis normalized axis
     */
    void rotate(glm::vec3 point, float angle, glm::vec3 axis);

    /**
     * Gets the rotated quaternion around axis.
     * @param angle the angle in degrees
     * @param axis the axis of rotation
     */
    static Quaternion &rotation(float angle, glm::vec3 axis);

    Quaternion &scalarMul(float s);

    Quaternion &square();

    double normSquared() const;

    double norm() const;

    Quaternion &normalize();

    Quaternion &conjugate();

    Quaternion &inverse();


    // Getters
    glm::vec4 getElements();

    double getScalar();

    glm::vec3 getVector();

    // Conversion methods
    glm::mat4 toMatrix() const;

    // Copy methods
    Quaternion &copy() const;
};


#endif //GAME_ENGINE_QUATERNION_HPP
