#ifndef SHADER_H
#define SHADER_H

#include <string>

class Shader {
public:
    unsigned int programId;
    bool debugging = false;
    std::string vertexShaderSrc;
    std::string fragmentShaderSrc;

    Shader(const std::string &vertexShader, const std::string &fragmentShader);

    ~Shader();

//    static unsigned int compileShader(unsigned int type, const std::string &source);

    void use() const;

    Shader &copy() const;
};

#endif /* SHADER_H */