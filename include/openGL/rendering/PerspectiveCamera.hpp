#ifndef GAME_ENGINE_PERSPECTIVECAMERA_HPP
#define GAME_ENGINE_PERSPECTIVECAMERA_HPP

#include <openGL/rendering/Camera.hpp>
#include <glm/glm.hpp>

class PerspectiveCamera : public Camera {
public:

    PerspectiveCamera(float fieldOfView, float aspectRatio, float near, float far);

    glm::mat4 getProjectionMatrix() const override;

    void setAspectRatio(float aspectRatio);

    glm::mat4 getViewMatrix() const override;

    glm::mat4 getWorldMatrix() const override;

private:
    float aspectRatio;
    float fov;
    float near;
    float far;

    glm::mat4 projectionMatrix;

    void updateProjectionMatrix() override;
};


#endif //GAME_ENGINE_PERSPECTIVECAMERA_HPP
