#ifndef GAME_ENGINE_CAMERA_HPP
#define GAME_ENGINE_CAMERA_HPP

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glad/glad.h>

// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    LOOK_UP,
    LOOK_LEFT,
    LOOK_RIGHT,
    LOOK_DOWN

};

// Default camera values
const GLfloat YAW = -15.01f;
const GLfloat PITCH = 0.2f;
const GLfloat SPEED = 10.5f;
const GLfloat SENSITIVTY = 0.10f;
const GLfloat ZOOM = 45.0f;

// An abstract camera class that processes input and calculates the corresponding Eular Angles, Vectors and Matrices for use in OpenGL
class Camera {
public:
    // Camera Attributes
    glm::vec3 Position;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    // Eular Angles
    GLfloat Yaw;
    GLfloat Pitch;
    // Camera options
    GLfloat MovementSpeed = SPEED;
    GLfloat MouseSensitivity = SENSITIVTY;
    GLfloat Zoom = ZOOM;
    GLfloat Floor = 0.0f;

    // Constructor with vectors
    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f),
           GLfloat yaw = YAW, GLfloat pitch = PITCH);

    // Constructor with scalar values
    Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch);

    // Returns the view matrix calculated using Eular Angles and the LookAt Matrix
    virtual glm::mat4 getViewMatrix() const = 0;

    virtual glm::mat4 getWorldMatrix() const = 0;

    // Processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
    void ProcessKeyboard(Camera_Movement direction, GLfloat deltaTime);

    // Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
    void ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true);

    // Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
    void ProcessMouseScroll(GLfloat yoffset);

    virtual glm::mat4 getProjectionMatrix() const = 0;


private:

    // Calculates the front vector from the Camera's (updated) Eular Angles
    void updateCameraVectors();

    void updateViewMatrices();

    virtual void updateProjectionMatrix() = 0;

protected:
    glm::mat4 worldTransform;
    glm::mat4 viewMatrix;

};

#endif /* GAME_ENGINE_CAMERA_HPP */
