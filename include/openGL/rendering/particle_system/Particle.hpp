#ifndef GAME_ENGINE_PARTICLE_HPP
#define GAME_ENGINE_PARTICLE_HPP

#include <glm/glm.hpp>
#include<string>

class Particle {
public:
    glm::vec3 position;
    glm::vec3 velocity;
    glm::vec4 color;

    float size;
    float life;

    Particle();

    std::string toString();
};


#endif //GAME_ENGINE_PARTICLE_HPP
