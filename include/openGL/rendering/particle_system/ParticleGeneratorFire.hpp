#ifndef GAME_ENGINE_PARTICLEGENERATORFIRE_HPP
#define GAME_ENGINE_PARTICLEGENERATORFIRE_HPP

#include <vector>
#include <openGL/objects/ParticleGenerator.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

class Material;

class IndexBuffer;

class ParticleGeneratorFire : public CoreParticleGenerator {
private:
    std::vector<float> quadDataPositions;
    std::vector<float> texData;
    glm::vec4 fireColor{0.5f, 0.1f, 1.0f, 1.0f};
    int currentNumOfParticles;
public:

    ParticleGeneratorFire(glm::vec3 location, int numOfParticles, Texture2D &texture,
                          ParticleRespawner &respawner, ParticleUpdater &updater,
                          glm::vec4 fireColor = glm::vec4{0.5f, 0.1f, 1.0f, 1.0f});

    void update(float ms) override;

    void draw(const Renderer &renderer, const Camera &camera) override;

    void updateGeometry();
};


#endif //GAME_ENGINE_PARTICLEGENERATORFIRE_HPP
