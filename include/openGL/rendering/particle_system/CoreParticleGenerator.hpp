#ifndef GAMEENGINE_COREPARTICLEGENERATOR_HPP
#define GAMEENGINE_COREPARTICLEGENERATOR_HPP

#include <openGL/objects/scene-graph/SceneNode.hpp>
#include <openGL/rendering/particle_system/ParticleRespawnRain.hpp>
#include <openGL/rendering/particle_system/ParticleUpdaterRain.hpp>
#include <vector>
#include <openGL/rendering/particle_system/Particle.hpp>

class Texture2D;

class ParticleRespawner;

class ParticleUpdater;

class CoreParticleGenerator : public SceneNode {
public:
    CoreParticleGenerator(glm::vec3 location, int numOfParticles, Texture2D &texture,
                          ParticleRespawner &spawner, ParticleUpdater &updater);

    int numOfNewParticles;

    virtual void update(float ms);

protected:
    glm::vec3 position;
    int numOfParticles;
    std::vector<Particle> particles;

    Texture2D &texture;
    int lastUsedParticle = 0;
    double colorDecay = 0.2;
    ParticleRespawner &respawner;
    ParticleUpdater &updater;

    int findUnusedParticle();

private:
    void init();
};


#endif //GAMEENGINE_COREPARTICLEGENERATOR_HPP
