#ifndef GAME_ENGINE_PARTICLEUPDATER_HPP
#define GAME_ENGINE_PARTICLEUPDATER_HPP

class Particle;

class ParticleUpdater {
public:
    virtual void update(Particle &p, float dt) = 0;
};


#endif //GAME_ENGINE_PARTICLEUPDATER_HPP
