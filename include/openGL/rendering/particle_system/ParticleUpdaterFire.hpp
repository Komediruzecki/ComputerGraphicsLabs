#ifndef GAME_ENGINE_PARTICLEUPDATERFIRE_HPP
#define GAME_ENGINE_PARTICLEUPDATERFIRE_HPP

#include <openGL/rendering/particle_system/ParticleUpdater.hpp>

class Particle;

class ParticleUpdaterFire : public ParticleUpdater {
public:
    ParticleUpdaterFire();

    void update(Particle &p, float dt) override;
};


#endif //GAME_ENGINE_PARTICLEUPDATERFIRE_HPP
