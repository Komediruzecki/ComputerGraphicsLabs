#ifndef GAME_ENGINE_PARTICLERESPAWNFIRE_HPP
#define GAME_ENGINE_PARTICLERESPAWNFIRE_HPP

#include <random>
#include <glm/vec3.hpp>
#include <openGL/rendering/particle_system/ParticleRespawner.hpp>

class Particle;

class ParticleRespawnFire : public ParticleRespawner {
private:
    std::mt19937 rng;
    std::uniform_real_distribution<float> distribution;
public:

    ParticleRespawnFire();

    void respawn(Particle &p, glm::vec3 offset) override;
};


#endif //GAME_ENGINE_PARTICLERESPAWNFIRE_HPP
