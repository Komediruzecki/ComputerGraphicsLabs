#ifndef GAME_ENGINE_PARTICLERESPAWNRAIN_HPP
#define GAME_ENGINE_PARTICLERESPAWNRAIN_HPP

#include <openGL/rendering/particle_system/ParticleRespawner.hpp>
#include <random>
#include <glm/vec3.hpp>

class Particle;

class ParticleRespawnRain : public ParticleRespawner {
private:
    std::mt19937 rng;
    std::uniform_int_distribution<int> distribution;
public:
    ParticleRespawnRain();

    void respawn(Particle &p, glm::vec3 offset) override;
};


#endif //GAME_ENGINE_PARTICLERESPAWNRAIN_HPP
