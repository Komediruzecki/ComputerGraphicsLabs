#ifndef GAME_ENGINE_PARTICLERESPAWNER_HPP
#define GAME_ENGINE_PARTICLERESPAWNER_HPP

#include <glm/vec3.hpp>

class Particle;

class ParticleRespawner {
public:
    virtual void respawn(Particle &p, glm::vec3 offset) = 0;

    virtual inline void doneInit() { initialized = true; };
protected:
    bool initialized = false;
};

#endif //GAME_ENGINE_PARTICLERESPAWNER_HPP
