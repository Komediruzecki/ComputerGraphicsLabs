#ifndef GAME_ENGINE_PARTICLEUPDATERRAIN_HPP
#define GAME_ENGINE_PARTICLEUPDATERRAIN_HPP

#include <openGL/rendering/particle_system/ParticleUpdater.hpp>

class Particle;

class ParticleUpdaterRain : public ParticleUpdater {
public:
    float colorDecay;

    ParticleUpdaterRain();

    void update(Particle &p, float dt) override;
};


#endif //GAME_ENGINE_PARTICLEUPDATERRAIN_HPP
