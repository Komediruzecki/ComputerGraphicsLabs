#ifndef GAMEENGINE_SCENEGRAPHRENDERER_HPP
#define GAMEENGINE_SCENEGRAPHRENDERER_HPP


#include<openGL/rendering/Renderer.hpp>

class SceneNode;

class Camera;


class SceneGraphRenderer : public Renderer {
public:
    void renderSceneGraph(SceneNode *node, const Camera &camera);

private:

    void drawNode(SceneNode *node, const Camera &camera);

};


#endif //GAMEENGINE_SCENEGRAPHRENDERER_HPP
