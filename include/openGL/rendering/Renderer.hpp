#ifndef GAME_ENGINE_RENDERER_HPP
#define GAME_ENGINE_RENDERER_HPP

// Could be Singleton but does not need to be if we want multiple such renderer's...
#include<vector>
#include <glm/vec3.hpp>

class Object3D;

class MeshObject;

class Scene;

class Camera;

class Material;

class SceneNode;

class Particle;

class BufferAttribute;

class Renderer {
public:
    Renderer();

    void render(const Scene &scene, const Camera &camera) const;

    void renderObject(Object3D &object, const Camera &camera) const;

    // Render objects methods
    void renderMesh(MeshObject &obj, const Camera &camera) const;

    void renderMesh(MeshObject &obj, const Camera &camera, Material &material) const;

    void renderParticles(MeshObject &mesh, glm::vec3 position, std::vector<Particle> particles,
                         const Camera &camera) const;

    void clear() const;

private:

    // helper functions
//    std::vector<float> combineAttributesData(std::map<std::string, BufferAttribute> map);
    void setUniforms(MeshObject &obj, const Camera &camera) const;

    std::vector<float> combineAttributeData(std::vector<BufferAttribute> data) const;

    void setUniforms(MeshObject &obj, const Camera &camera, Material &material) const;
};

#endif /* GAME_ENGINE_RENDERER_HPP */