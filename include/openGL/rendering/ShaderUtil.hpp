#ifndef SHADER_UTIL_H
#define SHADER_UTIL_H

#include <string>

class Material;

class ShaderUtil {
public:
    static std::string readShaderFromFile(const std::string &filePath);

    static unsigned int compileShader(unsigned int type, const std::string &source);

    static Material &
    createMaterialFromShaders(const std::string &vertexShaderPath, const std::string &fragmentShaderPath);
};

#endif /* SHADER_UTIL_H */