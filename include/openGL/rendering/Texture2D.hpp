#ifndef GAME_ENGINE_TEXTURE2D_HPP
#define GAME_ENGINE_TEXTURE2D_HPP

#include<string>
#include <glad/glad.h>

class Texture2D {
public:

    unsigned int texture;
    GLint width;
    GLint height;
    GLint internalFormat;
    GLint textureWrapT;
    GLint textureWrapS;
    GLint textureMinFilter;
    GLint textureMaxFilter;

    GLenum format;
    GLenum type;

    Texture2D();

    void generate(unsigned char *data, int width, int height);

    void bind();

    void unbind();
};


#endif //GAME_ENGINE_TEXTURE2D_HPP
