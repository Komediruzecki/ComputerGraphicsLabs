#ifndef GAME_ENGINE_PARTICLEGENERATOR_HPP
#define GAME_ENGINE_PARTICLEGENERATOR_HPP

#include <vector>
#include <random>
#include <glm/vec3.hpp>

#include <openGL/rendering/Texture2D.hpp>
#include <openGL/rendering/particle_system/ParticleRespawnRain.hpp>
#include <openGL/rendering/particle_system/ParticleUpdaterRain.hpp>
#include <openGL/rendering/particle_system/Particle.hpp>
#include <openGL/objects/scene-graph/SceneNode.hpp>
#include <openGL/rendering/particle_system/CoreParticleGenerator.hpp>

class VertexBuffer;

class VertexArray;

class ParticleUpdater;

class ParticleRespawner;

class Material;


class ParticleGenerator : public CoreParticleGenerator {
public:
    ParticleGenerator(glm::vec3 location, int numOfParticles, Texture2D &texture,
                      ParticleRespawner &spawner, ParticleUpdater &updater);

    void draw(const Renderer &renderer, const Camera &camera) override;

protected:
    void init();
};


#endif //GAME_ENGINE_PARTICLEGENERATOR_HPP
