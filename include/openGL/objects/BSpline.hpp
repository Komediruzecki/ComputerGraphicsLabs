#ifndef GAME_ENGINE_BSPLINE_H
#define GAME_ENGINE_BSPLINE_H

#include <string>
#include <vector>
#include <glm/mat4x4.hpp>
#include <glm/glm.hpp>
#include "Object3D.hpp"
#include "MeshObject.hpp"

class VertexBuffer;

class IndexBuffer;

class VertexArray;

class Material;

class BufferGeometry;

class BSpline : public MeshObject {
public:
    float granularity = 0.01;

    BSpline(std::vector<glm::vec3> controlPoints, glm::mat4 periodicSegmentMatrix);

    static BSpline &fromFile(const std::string &filePath);


    static glm::vec3
    evaluate(const std::vector<glm::vec3> &controlPoints, glm::mat4 periodicSegmentMatrix, float t, int segment);

    glm::vec3 evaluate(float t, int segment);

    glm::vec3 evaluateFirstDer(float t, int segment);

    glm::vec3 evaluateSecondDer(float t, int segment);

    glm::mat4 getControlPointsMatrix(int segment);

    static glm::mat4 getControlPointsMatrix(std::vector<glm::vec3> controlPoints, int segment);

    std::vector<glm::vec3> getControlPoints();

private:
    // Already multiplied by 1/6
    glm::mat4 periodicSegmentMatrix;
    std::vector<glm::vec3> controlPoints;

    std::vector<unsigned int> makeLineIndices(int numberOfPositions);

    Material &getDefaultMaterial();

    BufferGeometry &
    createGeometry(std::vector<glm::vec3> controlPoints, glm::mat4 periodicSegmentMatrix, float granularity);
};

#endif /* GAME_ENGINE_BSPLINE_H */