#ifndef GAMEENGINE_SCENENODE_HPP
#define GAMEENGINE_SCENENODE_HPP

#include<vector>
#include<string>
#include<glm/vec3.hpp>
#include<glm/detail/type_mat.hpp>
#include <glm/detail/type_mat4x4.hpp>

class Renderer;

class MeshObject;

class Camera;

class Material;

class SceneNode {
public:
    SceneNode(MeshObject *mesh = nullptr);

    ~SceneNode();

    void addChild(SceneNode *node);

    void removeChild(SceneNode *node);

    virtual void update(float ms);

    virtual void draw(const Renderer &renderer, const Camera &camera);

    std::vector<SceneNode *>::const_iterator getChildIteratorStart();

    std::vector<SceneNode *>::const_iterator getChildIteratorEnd();

    Material &getMaterial() const;

    void setTransform(const glm::mat4 &transform);

    void setModelScale(const glm::vec3 &modelScale);

    void setMaterial(Material *material);

    void setMesh(MeshObject *mesh);

    const glm::mat4 &getWorldTransform() const;

    const glm::mat4 &getTransform() const;

    const glm::vec3 &getModelScale() const;

    MeshObject *getMesh() const;

    void setId(const std::string &id);

    std::string toString() const;

protected:
    std::string id;
    unsigned int uid;
    SceneNode *parent;

    glm::mat4 worldTransform;
    glm::mat4 transform;
    glm::vec3 modelScale;

    // SceneNode does not care of deletion of this memory because others might  use it! todo: do it somewhere else
    MeshObject *mesh;
    Material *material;

    std::vector<SceneNode *> children;
};


#endif //GAMEENGINE_SCENENODE_HPP
