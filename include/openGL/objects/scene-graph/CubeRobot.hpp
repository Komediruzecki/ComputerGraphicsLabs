#ifndef GAMEENGINE_CUBEROBOT_HPP
#define GAMEENGINE_CUBEROBOT_HPP


#include<openGL/objects/scene-graph/SceneNode.hpp>
#include <openGL/objects/loaders/OBJ_Loader.hpp>
#include <openGL/objects/geometry/BufferGeometry.hpp>
#include <openGL/objects/materials/Material.hpp>
#include<openGL/objects/MeshObject.hpp>
#include <openGL/rendering/ShaderUtil.hpp>

class CubeRobot : public SceneNode {
public:
    CubeRobot();

    void update(float ms) override;


    static inline void createCube() {
        objl::Loader loader;
        bool loaded = loader.LoadFile("./assets/objects/cube.obj");

        BufferGeometry *cubeGeom;
        if (!loaded) {
            std::cout << "Could not load cube robot object!" << std::endl;
            throw std::invalid_argument("Cannot load cube robot!");
        } else {
            cubeGeom = &BufferGeometry::fromLoader(loader);
        }

        Material &material = ShaderUtil::createMaterialFromShaders("./assets/shaders/basic_mesh_shader/vertex.vert",
                                                                   "./assets/shaders/basic_mesh_shader/pixel.frag");
        // Materials
        cube = new MeshObject(*cubeGeom, material);

    }

    static inline void deleteCube() {
        delete cube;
    }

private:
    static MeshObject *cube;
    SceneNode *head;
    SceneNode *leftArm;
    SceneNode *rightArm;

};


#endif //GAMEENGINE_CUBEROBOT_HPP
