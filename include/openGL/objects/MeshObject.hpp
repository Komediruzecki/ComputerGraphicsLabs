#ifndef MESH_OBJECT_H
#define MESH_OBJECT_H

#include <openGL/objects/Object3D.hpp>
#include <openGL/objects/RenderType.hpp>
#include <glad/glad.h>

class BufferGeometry;

class Material;


class MeshObject : public Object3D {
public:
    // Should we allow only enums which are applicable to mesh object? (triangle, triangle strip/fan)
    GLenum drawMode;
    BufferGeometry &geometry;
    Material &material;

    MeshObject(BufferGeometry &geometry, Material &material);

    ~MeshObject() override;

    RenderType getType() const override;
};

#endif /* MESH_OBJECT_H */