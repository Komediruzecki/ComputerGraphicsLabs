#ifndef GAME_ENGINE_BUFFERGEOMETRY_HPP
#define GAME_ENGINE_BUFFERGEOMETRY_HPP


// todo: should string be included if is used in std::map
#include <string>
#include<vector>
#include <map>

#include <openGL/objects/loaders/OBJ_Loader.hpp>

class BufferAttribute;


class BufferGeometry {
public:
    std::map<std::string, BufferAttribute> attributes;
    std::string name;
    bool isInstanced = false;

    BufferGeometry(bool instanced);

    BufferGeometry();

    void addAttribute(const std::string &name, const BufferAttribute &attribute);

    static BufferGeometry &fromLoader(objl::Loader loader);

    std::vector<unsigned int> getIndices() const;

    void setIndices(std::vector<unsigned int> vector);

    BufferGeometry &copy() const;

private:
    // TODO: Maybe add some kind of type information, if it does not need to be unsigned int!
    std::vector<unsigned int> indices;
};


#endif //GAME_ENGINE_BUFFERGEOMETRY_HPP
