#ifndef GAME_ENGINE_LINEOBJECT_HPP
#define GAME_ENGINE_LINEOBJECT_HPP


#include <openGL/objects/geometry/BufferGeometry.hpp>
#include <glm/vec2.hpp>

class LineGeometry : public BufferGeometry {
public:
    LineGeometry(const glm::vec2 &startPoint, const glm::vec2 &endPoint);

};


#endif //GAME_ENGINE_LINEOBJECT_HPP
