#ifndef GAME_ENGINE_BUFFERATTRIBUTE_HPP
#define GAME_ENGINE_BUFFERATTRIBUTE_HPP

#include<vector>

class BufferAttribute {

public:
    unsigned int itemSize;
    // TODO: Template this
    std::vector<float> data;

    BufferAttribute();

    BufferAttribute(std::vector<float> data, unsigned int itemSize);

    const BufferAttribute &copy() const;
};


#endif //GAME_ENGINE_BUFFERATTRIBUTE_HPP
