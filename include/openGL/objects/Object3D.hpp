#ifndef GAME_ENGINE_OBJECT3D_HPP
#define GAME_ENGINE_OBJECT3D_HPP

#include <string>
#include <glm/vec3.hpp>
#include <glm/detail/type_mat.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <openGL/math/Quaternion.hpp>

#include <openGL/objects/RenderType.hpp>


class Object3D {
public:

    bool transformChanged = false;

    Object3D();

    virtual ~Object3D();

    // Helper methods
    void translate(glm::vec3 translation);

    /**
     * Multiplies components of scaler and current scale of this object.
     * @param scaler the scaler values
     */
    void multiplyScale(glm::vec3 scaler);

    void rotate(float angleDegrees, glm::vec3 axis);

    // Setters
    void setPosition(const glm::vec3 &position);

    void setScale(const glm::vec3 &scale);

    void setRotation(const Quaternion &q);

    void setRotation(float angle, glm::vec3 axis);

    /**
     * Sets the transforms (position, scale and rotation) to transform matrix and resets the transformations (position, scale rotation) to
     * identities.
     */
    void setTransformMatrix();

    /**
     * Sets the transform matrix to this object and resets all fields to identity positions (position, scale, rotation).
     * @param newTransform the new transform to apply to this object
     */
    void setTransformMatrix(glm::mat4 newTransform);

    // Combine methods
    // Apply Methods (for changing object mesh)
    /**
     * Combines current object transform matrix with specified one. Updates local transformation properties.
     * @param matrix the matrix to combine with current one
     */
    void combineMatrix(glm::mat4 matrix);

    /**
     * Changes object mesh as to reset object local transform to identities.
     */
    void applyMatrixToObject();

    // For type managing
    virtual RenderType getType() const = 0;

    glm::mat4 getModelMatrix() const;

private:
    // Identification properties
    int id;

    std::string uuid;
    // Transform properties
    glm::vec3 position;
    glm::vec3 scale;

    Quaternion rotation;

    // Full transform used by renderer
    glm::mat4 transformMatrix;

    void resetSimpleTransforms();
};


#endif //GAME_ENGINE_OBJECT3D_HPP
