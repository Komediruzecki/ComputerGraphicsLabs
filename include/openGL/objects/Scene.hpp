#ifndef GAME_ENGINE_SCENE_HPP
#define GAME_ENGINE_SCENE_HPP

#include <vector>
#include <openGL/objects/Object3D.hpp>

class Scene : public Object3D {

public:
    Scene();

    ~Scene() override;

    void add(Object3D *obj);

    std::vector<Object3D *> getObjects() const;

    RenderType getType() const override;

private:
    std::vector<Object3D *> objects;
};


#endif //GAME_ENGINE_SCENE_HPP
