#ifndef VERTEX_H
#define VERTEX_H

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <vector>

class Vertex {
public:
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoord;
    std::vector<float> vertexData;

    Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 texCoord);

    Vertex(glm::vec3 position);
};

#endif /* VERTEX_H */