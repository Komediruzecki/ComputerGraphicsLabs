#ifndef GAME_ENGINE_BASICMESHMATERIAL_HPP
#define GAME_ENGINE_BASICMESHMATERIAL_HPP


#include <openGL/objects/materials/Material.hpp>

class Shader;

class BasicMeshMaterial : public Material {
public:

    BasicMeshMaterial();

    BasicMeshMaterial(Shader &shader);

    const Shader &getShader();
};


#endif //GAME_ENGINE_BASICMESHMATERIAL_HPP
