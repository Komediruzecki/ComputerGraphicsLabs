#ifndef GAME_ENGINE_MATERIAL_HPP
#define GAME_ENGINE_MATERIAL_HPP

#include<glm/glm.hpp>

#include <unordered_map>
#include <iostream>
#include <map>
#include <any>
#include <string>

#include<openGL/rendering/Shader.hpp>

enum MaterialType {
    MATERIAL = 0,
    MESH_MATERIAL,
    LINE_MATERIAL,
};

class Material {
public:
    // Attributes of generic material
    glm::vec3 color = glm::vec3(1.0, 0.0, 0.0);

    std::unordered_map<std::string, int> uniformLocationsCache;
    std::map<std::string, std::any> uniforms;
    const Shader &shader;

    Material();

    Material(const Shader &shader, std::map<std::string, std::any> uniforms);

    explicit Material(const Shader &shader);

    void setUniform(const std::string &key, const std::any &value);

    void setUniformDefault(const std::string &key, const std::any &value);

    void setMat4(const std::string &name, const glm::mat4 &mat);

    void setVec4(const std::string &name, const glm::vec4 &vec);

    void setVec3(const std::string &name, const glm::vec3 &vec);

    void setFloat(const std::string &name, float value);

    void setBool(const std::string &name, bool value);

    void setInt(const std::string &name, int value);

    MaterialType getType() const;

    Material &copy() const;

private:
    const Shader &getShader() const;

    int getUniformLocation(const std::string &name);
};

#endif /* GAME_ENGINE_MATERIAL_HPP */