#ifndef GAME_ENGINE_IDLEEVENTSTATE_HPP
#define GAME_ENGINE_IDLEEVENTSTATE_HPP

#include<openGL/client/state-design/EventState.hpp>
#include<glm/glm.hpp>

class Renderer;

class Object3D;

class Camera;

class IdleEventState : public EventState {
    inline void onKeyPress(int key, int scanCode, int action, int mode) override {

    }

    inline void onKeyRelease(int key, int scanCode, int action, int mode) override {

    }

    inline void onClick() override {

    }

    inline void onClickRelease() override {

    }

    inline void onMouseMove(double x, double y) override {

    }

    inline void onMouseClick(glm::vec2 mousePos, int button, int action, int mods) override {

    }

    inline void onKeyDown() override {

    }

    inline void onKeyUp() override {

    }

    inline void afterDraw(Renderer &renderer, Object3D &obj) override {
    }

    inline void afterDraw(Renderer &renderer, const Camera &camera) override {

    }

    inline void onLeaving() override {

    }
};

#endif //GAME_ENGINE_IDLEEVENTSTATE_HPP
