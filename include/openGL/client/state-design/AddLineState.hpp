#ifndef GAME_ENGINE_ADDLINESTATE_HPP
#define GAME_ENGINE_ADDLINESTATE_HPP

#include <openGL/client/state-design/IdleEventState.hpp>
#include <glm/vec2.hpp>

class Scene;

class Object3D;

class Camera;

class Renderer;


class AddLineState : public IdleEventState {

public:

    void afterDraw(Renderer &r, const Camera &camera) override;

//    AddLineState(Scene &scene, const Object3D &prototype);

    AddLineState(Scene &scene);

    void onMouseMove(double x, double y) override;

    void onMouseClick(glm::vec2 mousePos, int button, int action, int mods) override;

    void reset();

private:
//    const Object3D &prototype;
    Scene &scene;
    bool firstClick;

    glm::vec2 firstPoint;
    glm::vec2 secondPoint;

};


#endif //GAME_ENGINE_ADDLINESTATE_HPP
