#ifndef GAME_ENGINE_EVENTMANAGER_HPP
#define GAME_ENGINE_EVENTMANAGER_HPP


class EventState;

class EventManager {

public:
    EventState &currentState;

    inline EventManager(EventState &state) : currentState(state) {}

};

#endif //GAME_ENGINE_EVENTMANAGER_HPP
