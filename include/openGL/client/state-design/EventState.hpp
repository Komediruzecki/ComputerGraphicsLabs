#ifndef GAME_ENGINE_EVENTSTATE_HPP
#define GAME_ENGINE_EVENTSTATE_HPP


#include <glm/vec2.hpp>

class Renderer;

class Object3D;

class Camera;

class EventState {
public:

    virtual void onKeyPress(int key, int scanCode, int action, int mode) = 0;

    virtual void onKeyRelease(int key, int scanCode, int action, int mode) = 0;

    virtual void onClick() = 0;

    virtual void onClickRelease() = 0;

    virtual void onMouseMove(double x, double y) = 0;

    virtual void onMouseClick(glm::vec2 mousePos, int button, int action, int mods) = 0;

    virtual void onKeyDown() = 0;

    virtual void onKeyUp() = 0;

    // Maybe
    virtual void afterDraw(Renderer &renderer, Object3D &obj) = 0;

    virtual void afterDraw(Renderer &renderer, const Camera &camera) = 0;

    virtual void onLeaving() = 0;
};


#endif //GAME_ENGINE_EVENTSTATE_HPP
