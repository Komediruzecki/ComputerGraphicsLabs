#ifndef GAME_ENGINE_SCENEMANAGER_HPP
#define GAME_ENGINE_SCENEMANAGER_HPP

class Renderer;

class Scene;

#include <openGL/rendering/PerspectiveCamera.hpp>
#include <openGL/client/state-design/EventState.hpp>

class SceneManager {

public:
    SceneManager(Renderer &renderer, Scene &scene);

    void update(double deltaTime);

    void onMouseMove(float xOffset, float yOffset);

    void onMouseScroll(float yOffset);

    void onWindowResize(int newWidth, int newHeight);

    void onKeyPress(int key, int mods);

    void onKeyRelease(int key, int mods);

    void mouseMove(double x, double y);

    void onMouseClick(glm::vec2 mousePosition, int button, int action, int mods);

    const Camera &getCamera();

private:
    PerspectiveCamera camera;
    Renderer &renderer;
    Scene &scene;

    EventState *state;

    PerspectiveCamera buildCamera();

    bool keys[1024];

    void doMovement(float deltaTime);

    void updateState(double time);
};


#endif //GAME_ENGINE_SCENEMANAGER_HPP
