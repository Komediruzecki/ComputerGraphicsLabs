#include <openGL/objects/materials/Material.hpp>
#include <openGL/rendering/Shader.hpp>

#include <utility>
#include <map>
#include <string>
#include <any>

#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>
#include <glad/glad.h>
#include <openGL/rendering/ShaderUtil.hpp>

Material::Material() : shader(getShader()) {
}


const Shader &Material::getShader() const {
    std::string vertexShader = ShaderUtil::readShaderFromFile("./assets/shaders/basic/vertex.vert");
    std::string fragmentShader = ShaderUtil::readShaderFromFile("./assets/shaders/basic/pixel.frag");
    Shader *sh = new Shader{vertexShader, fragmentShader};
    return *sh;
}

Material::Material(const Shader &shader) : shader(shader) {}


Material::Material(const Shader &shader, std::map<std::string, std::any> uniforms) :
        uniforms(std::move(uniforms)), shader(shader) {
}

int Material::getUniformLocation(const std::string &name) {
    if (uniformLocationsCache.find(name) != uniformLocationsCache.end()) {
        return uniformLocationsCache[name];
    } else {
        int location = glGetUniformLocation(shader.programId, name.c_str());
        if (location == -1) {
            std::cout << "[Warning] The " << name << " does not exist for shader " << shader.programId << "."
                      << std::endl;
        }

        uniformLocationsCache[name] = location;
        return location;
    }
}

void Material::setMat4(const std::string &name, const glm::mat4 &mat) {
    glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, &mat[0][0]);
}

void Material::setVec4(const std::string &name, const glm::vec4 &vec) {
    glUniform4fv(getUniformLocation(name), 1, glm::value_ptr(vec));
}

void Material::setVec3(const std::string &name, const glm::vec3 &vec) {
    glUniform3fv(getUniformLocation(name), 1, glm::value_ptr(vec));
}

void Material::setBool(const std::string &name, bool value) {
    glUniform1i(getUniformLocation(name), value);
}

void Material::setFloat(const std::string &name, float value) {
    glUniform1f(getUniformLocation(name), value);
}


void Material::setInt(const std::string &name, int value) {
    glUniform1i(getUniformLocation(name), value);
}

Material &Material::copy() const {
    // TODO: not copying cache
    std::map<std::string, std::any> copiedUniforms = uniforms;
    Material *material = new Material(shader, copiedUniforms);

    return *material;
}

MaterialType Material::getType() const {
    return MaterialType::MATERIAL;
}

void Material::setUniform(const std::string &key, const std::any &value) {
    uniforms[key] = value;
}

void Material::setUniformDefault(const std::string &key, const std::any &value) {
    if (uniforms.find(key) == uniforms.end()) {
        setUniform(key, value);
    }
}
