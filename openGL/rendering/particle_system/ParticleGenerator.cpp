#include <openGL/objects/ParticleGenerator.hpp>
#include <glad/glad.h>
#include <iostream>
#include <random>
#include <functional>
#include <vector>

#include <glm/glm.hpp>
#include <openGL/buffers/VertexBufferLayout.hpp>
#include <openGL/buffers/VertexArray.hpp>
#include <openGL/buffers/VertexBuffer.hpp>
#include <openGL/objects/materials/Material.hpp>
#include <openGL/rendering/Shader.hpp>
#include <openGL/rendering/Texture2D.hpp>
#include <openGL/rendering/particle_system/ParticleRespawnRain.hpp>
#include <openGL/rendering/particle_system/ParticleUpdaterRain.hpp>
#include <openGL/rendering/particle_system/Particle.hpp>
#include <openGL/objects/geometry/BufferGeometry.hpp>
#include <openGL/objects/geometry/BufferAttribute.hpp>
#include <openGL/rendering/ShaderUtil.hpp>
#include <openGL/objects/MeshObject.hpp>
#include <openGL/rendering/Renderer.hpp>
#include <openGL/rendering/Camera.hpp>

ParticleGenerator::ParticleGenerator(glm::vec3 location, int numOfParticles, Texture2D &texture,
                                     ParticleRespawner &respawner, ParticleUpdater &updater) : CoreParticleGenerator(
        location, numOfParticles, texture, respawner, updater) {
    init();
}

void ParticleGenerator::init() {
    std::vector<float> vertexDataPos = {
            // x, y
            -1.0f, -1.0f,
            1.0f, -1.0f,
            1.0f, 1.0f,

            -1.0f, -1.0f,
            1.0f, 1.0f,
            -1.0f, 1.0f
    };

    std::vector<float> vertexDataTex = {
            // tex_x, tex_y
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f
    };

    BufferGeometry *geom = new BufferGeometry();
    BufferAttribute positionAttr = BufferAttribute(vertexDataPos, 2);
    BufferAttribute texAttr = BufferAttribute(vertexDataTex, 2);
    geom->addAttribute("vertexPosition", positionAttr);
    geom->addAttribute("vertexTexCoord", texAttr);

    Material &material = ShaderUtil::createMaterialFromShaders("./assets/shaders/particle/vertex.vert",
                                                               "./assets/shaders/particle/pixel.frag");
    setMesh(new MeshObject(*geom, material));
    setMaterial(&material);
}


void ParticleGenerator::draw(const Renderer &renderer, const Camera &camera) {
    // Move state code into renderer pipeline (based on properties and stuff in materials...)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    mesh->material.shader.use();
    texture.bind();
    renderer.renderParticles(*getMesh(), position, particles, camera);
    texture.unbind();
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


