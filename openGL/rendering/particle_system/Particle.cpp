#include <openGL/rendering/particle_system/Particle.hpp>
#include<openGL/EngineCore.hpp>

Particle::Particle() : position(0.0), velocity(0.0), color(1.0), life(-1.0f), size(1.0f) {


}

std::string Particle::toString() {
    return "pos: " + glm::to_string(position) + " vel" + glm::to_string(velocity) + "col" + glm::to_string(color) +
           "life(" + std::to_string(life) + ")" + "size(" + std::to_string(size) + ")";
}
