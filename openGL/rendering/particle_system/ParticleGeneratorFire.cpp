#include <openGL/rendering/particle_system/ParticleGeneratorFire.hpp>
#include <openGL/buffers/IndexBuffer.hpp>
#include <openGL/objects/materials/Material.hpp>
#include <openGL/rendering/Shader.hpp>
#include <openGL/rendering/Texture2D.hpp>

#include <openGL/buffers/VertexArray.hpp>
#include <openGL/buffers/VertexBuffer.hpp>
#include <openGL/buffers/VertexBufferLayout.hpp>
#include <openGL/rendering/particle_system/Particle.hpp>
#include <openGL/rendering/particle_system/ParticleRespawner.hpp>
#include <openGL/rendering/particle_system/ParticleUpdater.hpp>

#include <glm/glm.hpp>
#include <iostream>
#include <openGL/objects/MeshObject.hpp>
#include <openGL/rendering/ShaderUtil.hpp>
#include <openGL/objects/geometry/BufferAttribute.hpp>
#include <openGL/objects/geometry/BufferGeometry.hpp>
#include <openGL/rendering/Renderer.hpp>
#include <openGL/rendering/Camera.hpp>

ParticleGeneratorFire::ParticleGeneratorFire(glm::vec3 location, int numOfParticles, Texture2D &texture,
                                             ParticleRespawner &respawner, ParticleUpdater &updater,
                                             glm::vec4 fireColor)
        : CoreParticleGenerator(location, numOfParticles, texture, respawner, updater), fireColor(fireColor) {


    this->quadDataPositions = {
            -1.0f, -1.0f,
            1.0f, -1.0f,
            1.0f, 1.0f,
            -1.0f, 1.0f,
    };
    this->texData = {
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f
    };


    numOfNewParticles = 0;
    for (Particle &p : particles) {
        respawner.respawn(p, glm::vec3(0, 0, 0));
    }

    respawner.doneInit();
    updateGeometry();
}


void ParticleGeneratorFire::update(float ms) {
    CoreParticleGenerator::update(ms);
}

void ParticleGeneratorFire::draw(const Renderer &renderer, const Camera &camera) {
    glBlendFunc(GL_ONE, GL_ONE);
    Material &material = mesh->material;
    material.shader.use();
    texture.bind();
    material.setUniform("viewMatrix", camera.getViewMatrix());
    material.setUniform("fire_origin", this->position);
    material.setUniform("fire_color", this->fireColor);
    material.setUniform("fire_atlas", 0);
    renderer.renderMesh(*mesh, camera);
    texture.unbind();
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


void ParticleGeneratorFire::updateGeometry() {
    std::vector<unsigned int> vertexIndices;
    std::vector<float> vertexDataLife;
    std::vector<float> vertexDataTex;
    std::vector<float> vertexDataPos;
    std::vector<float> vertexDataOffset;
    std::vector<float> vertexDataVelocity;

    this->currentNumOfParticles = 0;
    std::vector<int> indexes = {0, 1, 2, 0, 2, 3};
    for (int i = 0; i < numOfParticles; i++) {
        Particle p = particles[i];
        if (p.life < 0.0) continue;
        this->currentNumOfParticles += 1;
        for (int j = 0; j < 4; j++) {
            // Lifetime
            vertexDataLife.push_back(p.life);

            // TexCoords
            vertexDataTex.push_back(this->texData[j * 2]);
            vertexDataTex.push_back(this->texData[j * 2 + 1]);

            // Corners
            vertexDataPos.push_back(this->quadDataPositions[j * 2]);
            vertexDataPos.push_back(this->quadDataPositions[j * 2 + 1]);

            // CenterOffsets
            vertexDataOffset.push_back(p.position.x);
            vertexDataOffset.push_back(p.position.y);
            vertexDataOffset.push_back(p.position.z);

            // Velocities
            vertexDataVelocity.push_back(p.velocity.x);
            vertexDataVelocity.push_back(p.velocity.y);
            vertexDataVelocity.push_back(p.velocity.z);
        }

        for (int idx : indexes) {
            vertexIndices.push_back(idx + 4 * i);
        }
    }

    BufferGeometry *geom = new BufferGeometry(true);
    BufferAttribute lifeAttribute = BufferAttribute(vertexDataLife, 1);
    BufferAttribute textureAttribute = BufferAttribute(vertexDataTex, 2);
    BufferAttribute positionAttribute = BufferAttribute(vertexDataPos, 2);
    BufferAttribute offsetsAttribute = BufferAttribute(vertexDataOffset, 3);
    BufferAttribute velocityAttribute = BufferAttribute(vertexDataVelocity, 3);
    geom->addAttribute("particleLifetime", lifeAttribute);
    geom->addAttribute("vertexTexCoord", textureAttribute);
    geom->addAttribute("vertexPosition", positionAttribute);
    geom->addAttribute("particleOffset", offsetsAttribute);
    geom->addAttribute("particleVelocity", velocityAttribute);
    geom->setIndices(vertexIndices);

    if (!mesh) {
        Material &material = ShaderUtil::createMaterialFromShaders("./assets/shaders/particle/vertex_fire.vert",
                                                                   "./assets/shaders/particle/pixel_fire.frag");
        setMesh(new MeshObject(*geom, material));
        setMaterial(&material);
        mesh->drawMode = GL_TRIANGLES;
    } else {
        mesh->geometry = *geom;
    }
}
