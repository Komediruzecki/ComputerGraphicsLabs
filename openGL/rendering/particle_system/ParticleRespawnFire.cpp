#include <openGL/rendering/particle_system/ParticleRespawnFire.hpp>
#include <openGL/rendering/particle_system/Particle.hpp>
#include <glm/vec3.hpp>

#include <functional>
#include <iostream>
#include <random>

ParticleRespawnFire::ParticleRespawnFire() {
    std::random_device rd;     // only used once to initialise (seed) engine
    this->rng = std::mt19937{rd()};    // random-number engine used (Mersenne-Twister in this case)
    this->distribution = std::uniform_real_distribution<float>(0.0f, 1.0f);
}

void ParticleRespawnFire::respawn(Particle &p, glm::vec3 offset) {
    if (initialized) {
        return;
    }

    auto dice = std::bind(distribution, std::ref(this->rng));
    float diameterAroundCenter = 0.5f;
    float halfDiameterAroundCenter = diameterAroundCenter / 2.0f;

    float xStartOffset = diameterAroundCenter * dice() - halfDiameterAroundCenter;
    xStartOffset /= 3.0f;

    float yStartOffset = diameterAroundCenter * dice() - halfDiameterAroundCenter;
    yStartOffset /= 10.0f;

    float zStartOffset = diameterAroundCenter * dice() - halfDiameterAroundCenter;
    zStartOffset /= 3.0f;

    float upVelocity = 0.1f * dice();

    float xSideVelocity = 0.02f * dice();
    if (xStartOffset > 0.0) {
        xSideVelocity *= -1.0;
    }

    float zSideVelocity = 0.02f * dice();
    if (zStartOffset > 0.0) {
        zSideVelocity *= -1.0;
    }

    p.life = 8.0f * dice();
    p.velocity = glm::vec3(xSideVelocity, upVelocity, zSideVelocity);
    p.position = glm::vec3(xStartOffset, (yStartOffset + abs(xStartOffset / 2.0f)), zStartOffset);
//    p.color = glm::vec4(0.8f, 0.25f, 0.25f, 1.0f);
}
