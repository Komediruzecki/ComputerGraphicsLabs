#include<openGL/rendering/particle_system/CoreParticleGenerator.hpp>
#include<glm/glm.hpp>
#include <openGL/rendering/particle_system/ParticleRespawner.hpp>
#include <openGL/rendering/particle_system/ParticleUpdater.hpp>
#include <openGL/rendering/particle_system/Particle.hpp>
#include <openGL/rendering/Texture2D.hpp>
#include <openGL/rendering/particle_system/CoreParticleGenerator.hpp>
#include <iostream>


CoreParticleGenerator::CoreParticleGenerator(glm::vec3 location, int numOfParticles, Texture2D &texture,
                                             ParticleRespawner &spawner, ParticleUpdater &updater) :
        position(location),
        numOfParticles(numOfParticles),
        texture(texture),
        respawner(spawner),
        updater(updater) {
    init();
}

void CoreParticleGenerator::init() {
    for (int c = 0; c < numOfParticles; c++) {
        Particle p{};
        p.size = 0.5;
        particles.push_back(p);
    }

    numOfNewParticles = 1;
}

void CoreParticleGenerator::update(float ms) {
    // New particles
    for (int c = 0; c < numOfNewParticles; c++) {
        int unusedParticleIdx = findUnusedParticle();
        respawner.respawn(this->particles[unusedParticleIdx], glm::vec3(0.0, 1.0, 0.0));
    }

    for (Particle &particle : particles) {
        if (particle.life > 0.0f) {
            updater.update(particle, ms);
        }
    }
}


int CoreParticleGenerator::findUnusedParticle() {
    for (int c = lastUsedParticle; c < numOfParticles; c++) {
        if (particles[c].life <= 0.0) {
            lastUsedParticle = c;
            return c;
        }
    }

    for (int c = 0; c < numOfParticles; c++) {
        if (particles[c].life <= 0.0) {
            return c;
        }
    }

    // TODO: Delete in prod
    std::cout << "Not good, consider updating particle size!" << std::endl;
    lastUsedParticle = 0;
    return 0;
}
