#include <openGL/rendering/particle_system/ParticleRespawnRain.hpp>
#include <openGL/rendering/particle_system/Particle.hpp>

#include <functional>
#include <random>
#include <glm/vec3.hpp>

ParticleRespawnRain::ParticleRespawnRain() {
    std::random_device rd;     // only used once to initialise (seed) engine
    this->rng = std::mt19937{rd()};    // random-number engine used (Mersenne-Twister in this case)
    this->distribution = std::uniform_int_distribution<int>(0, 100);
}

void ParticleRespawnRain::respawn(Particle &p, glm::vec3 offset) {
    auto dice = std::bind(distribution, std::ref(this->rng));

    float random = (dice() - 50) / 10.0f;
    double r2 = 0.5 + (dice() / 100.0f);
    double r3 = 0.5 + (dice() / 100.0f);
    double r1 = 0.5 + (dice() / 100.0f);
    p.position = random + offset;
    p.color = glm::vec4(r1, r2, r3, 1.0f);

    // For one life ~= 100 particles and creation of 2 each dt
    p.life = 8.0f;
    p.velocity = glm::vec3(dice() / 100.0f > 0.5 ? 2.0f : -2.0, -3.0f, 0.0f);
}
