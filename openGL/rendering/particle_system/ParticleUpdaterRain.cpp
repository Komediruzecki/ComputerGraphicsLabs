#include <openGL/rendering/particle_system/ParticleUpdaterRain.hpp>
#include <openGL/rendering/particle_system/Particle.hpp>

ParticleUpdaterRain::ParticleUpdaterRain() : colorDecay(0.5) {

}

void ParticleUpdaterRain::update(Particle &particle, float dt) {
    particle.position += particle.velocity * dt;
    particle.color.a -= dt * this->colorDecay;
    particle.life -= dt;
}
