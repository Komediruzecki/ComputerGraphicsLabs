#include <openGL/rendering/ShaderUtil.hpp>
#include <glad/glad.h>

#include <fstream>
#include <sstream>
#include <iostream>
#include <openGL/rendering/Shader.hpp>
#include <openGL/objects/materials/Material.hpp>

std::string ShaderUtil::readShaderFromFile(const std::string &filePath) {
    std::ifstream stream{filePath};

    std::stringstream shader;
    std::string line;
    while (getline(stream, line)) {
        shader << line << std::endl;
    }

    return shader.str();
}

unsigned int ShaderUtil::compileShader(unsigned int type, const std::string &source) {
    unsigned int id = glCreateShader(type);

    // Make sure *src points to valid data, until this is compiled...
    // If function returns an string make a copy of it with lvalue std::string copy = func();
    const char *src = source.c_str();
    glShaderSource(id, 1, &src, nullptr);
    glCompileShader(id);

    int result;
    glGetShaderiv(id, GL_COMPILE_STATUS, &result);

    if (result == GL_FALSE) {
        int length;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);

        char *message = (char *) malloc(length * sizeof(char));
        glGetShaderInfoLog(id, length, &length, message);

        std::cout << "Failed to compile " << ((type == GL_VERTEX_SHADER) ? "vertex" : "fragment") << " shader: "
                  << std::endl;
        std::cout << message << std::endl;

        // Delete and return 0 (cannot -1)
        glDeleteShader(id);
        return 0;
    }

    return id;
}

Material &
ShaderUtil::createMaterialFromShaders(const std::string &vertexShaderPath, const std::string &fragmentShaderPath) {
    // Setup shader's
    std::string vertexShader = ShaderUtil::readShaderFromFile(vertexShaderPath);
    std::string fragmentShader = ShaderUtil::readShaderFromFile(fragmentShaderPath);
    Shader *shader = new Shader{vertexShader, fragmentShader};
    // Materials
    return *(new Material(*shader));
}
