#include <openGL/rendering/Texture2D.hpp>
#include <glad/glad.h>
#include <iostream>

Texture2D::Texture2D()
        : width(0), height(0), internalFormat(GL_RGBA), format(GL_RGBA),
          type(GL_UNSIGNED_BYTE), textureWrapS(GL_REPEAT), textureWrapT(GL_REPEAT), textureMinFilter(GL_LINEAR),
          textureMaxFilter(GL_LINEAR) {
    glGenTextures(1, &texture);
}

void Texture2D::generate(unsigned char *data, GLint width, GLint height) {

    this->width = width;
    this->height = height;
    bind();

    // set the texture wrapping/filtering options (on the currently bound texture object)
    // set Wrapping to CLAMP_TO_EDGE if using alpha textures
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, this->textureWrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, this->textureWrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, this->textureMinFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, this->textureMaxFilter);

    if (data) {
        glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, type, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    } else {
        std::cout << "Failed to load texture" << std::endl;
    }

    unbind();
}

void Texture2D::bind() {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
}

void Texture2D::unbind() {
    glBindTexture(GL_TEXTURE_2D, 0);
}

