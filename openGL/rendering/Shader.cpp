#include <openGL/rendering/Shader.hpp>
#include <openGL/rendering/ShaderUtil.hpp>

#include <iostream>
#include <glad/glad.h>

Shader::Shader(const std::string &vertexShader, const std::string &fragmentShader) : vertexShaderSrc(vertexShader),
                                                                                     fragmentShaderSrc(fragmentShader) {

    this->programId = glCreateProgram();
    unsigned int vs = ShaderUtil::compileShader(GL_VERTEX_SHADER, vertexShader);
    unsigned int fs = ShaderUtil::compileShader(GL_FRAGMENT_SHADER, fragmentShader);

    glAttachShader(programId, vs);
    glAttachShader(programId, fs);
    glLinkProgram(programId);
    glValidateProgram(programId);

    // Detach shader, deletes the program..
    // Should be called, deletes source
    // Do not do for debugging purposes
    if (!debugging) {
        glDetachShader(programId, vs);
        glDetachShader(programId, fs);
    }

    glDeleteShader(vs);
    glDeleteShader(fs);
}

Shader::~Shader() {
    glDeleteProgram(programId);
}

void Shader::use() const {
    glUseProgram(programId);
}

Shader &Shader::copy() const {
    Shader *sh = new Shader(vertexShaderSrc, fragmentShaderSrc);
    return *sh;
}
