#include<openGL/rendering/SceneGraphRenderer.hpp>
#include<openGL/objects/scene-graph/SceneNode.hpp>
#include<openGL/objects/MeshObject.hpp>
#include <glm/detail/type_mat.hpp>
#include <openGL/math/matrix/MatrixUtil.hpp>
#include <openGL/rendering/SceneGraphRenderer.hpp>
#include <openGL/EngineCore.hpp>

void SceneGraphRenderer::renderSceneGraph(SceneNode *node, const Camera &camera) {
    drawNode(node, camera);
}

void SceneGraphRenderer::drawNode(SceneNode *node, const Camera &camera) {
    if (node->getMesh()) {
        glm::mat4 nodeTransform = node->getWorldTransform() * MatrixUtil::scale(node->getModelScale());
        node->getMesh()->setTransformMatrix(nodeTransform);
        node->draw(*this, camera);
    }

    for (auto it = node->getChildIteratorStart(); it != node->getChildIteratorEnd(); it++) {
        drawNode(*it, camera);
    }
}


