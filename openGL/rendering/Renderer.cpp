#include <openGL/rendering/Renderer.hpp>
#include <openGL/rendering/Camera.hpp>
#include <openGL/objects/materials/Material.hpp>
#include <openGL/objects/MeshObject.hpp>
#include <openGL/objects/Scene.hpp>
#include <openGL/objects/geometry/BufferAttribute.hpp>
#include <openGL/objects/geometry/BufferGeometry.hpp>
#include <openGL/buffers/VertexArray.hpp>
#include <openGL/buffers/VertexBuffer.hpp>
#include <openGL/buffers/VertexBufferLayout.hpp>
#include <openGL/buffers/IndexBuffer.hpp>
#include <openGL/rendering/Shader.hpp>
#include <openGL/objects/scene-graph/SceneNode.hpp>

#include <openGL/rendering/particle_system/Particle.hpp>
#include <vector>
#include <glad/glad.h>
#include <openGL/EngineCore.hpp>


Renderer::Renderer() {
}


void Renderer::clear() const {
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::render(const Scene &scene, const Camera &camera) const {
    clear();
    for (Object3D *obj : scene.getObjects()) {
        renderObject(*obj, camera);
    }
}

void Renderer::renderObject(Object3D &object, const Camera &camera) const {
    if (object.getType() == RenderType::MESH_OBJECT) {
        auto &mesh = dynamic_cast<MeshObject &> (object);
        renderMesh(mesh, camera);
    }
}

void Renderer::renderMesh(MeshObject &mesh, const Camera &camera) const {
    renderMesh(mesh, camera, mesh.material);
}


void Renderer::setUniforms(MeshObject &mesh, const Camera &camera) const {
    setUniforms(mesh, camera, mesh.material);
}

void Renderer::renderMesh(MeshObject &mesh, const Camera &camera, Material &material) const {
    material.shader.use();

    // Init some buffers
    const auto indices = mesh.geometry.getIndices();
    VertexArray vao{};
    IndexBuffer ibo{&indices[0], static_cast<unsigned int>(indices.size())};
    VertexBufferLayout layout{};

    vao.bind();
    ibo.bind();

    // Apply uniforms, until we decide how will uniforms be set, this will be nothing
    // User sets the uniforms for their shader i.e. their object from outside, we just render it
    setUniforms(mesh, camera, material);

    // Get what shader have and iterate over these keys!
    auto geometryAttributes = mesh.geometry.attributes;

    GLint i;
    GLint count;

    GLint size; // size of the variable
    GLenum type; // type of the variable (float, vec3 or mat4, etc)

    const GLsizei bufSize = 30; // maximum name length
    GLchar name[bufSize]; // variable name in GLSL
    GLsizei length; // name length

    unsigned int program = material.shader.programId;

    glGetProgramiv(program, GL_ACTIVE_ATTRIBUTES, &count);
//    printf("Active Attributes: %d\n", count);


    std::vector<BufferAttribute> attributesData;
    unsigned int numberOfAttributeItems = 0;
    for (i = 0; i < count; i++) {
        glGetActiveAttrib(program, (GLuint) i, bufSize, &length, &size, &type, name);
//        printf("Attribute #%d Type: %u Name: %s\n", i, type, name);

        if (geometryAttributes.find(name) == geometryAttributes.end()) {
            // But do not throw if name starts with "gl" prefix.
            std::string attribName = std::string(name);
            if (attribName.find("gl") != 0) {
                printf("Attribute: %s not found\n", name);
                throw std::invalid_argument("No such attribute found!");
            }
        } else {
            BufferAttribute attribute = geometryAttributes[name];

            attributesData.push_back(attribute);
//            vertexData.insert(vertexData.end(), attribute.data.begin(), attribute.data.end());
//            printf("Attribute #%d Type: %u Name: %s\n", i, type, name);
            // TODO: This should be the type of attribute (or attribute would be some kind of templated)

            if ((attribute.itemSize == 3) && (type == GL_FLOAT_VEC3)) {
                layout.push<float>(attribute.itemSize);
            } else if ((attribute.itemSize == 1) && (type == GL_INT)) {
                layout.push<int>(attribute.itemSize);
            } else if ((attribute.itemSize == 2) && (type == GL_FLOAT_VEC2)) {
                layout.push<float>(attribute.itemSize);
            } else if ((attribute.itemSize == 1) && (type == GL_FLOAT)) {
                layout.push<float>(attribute.itemSize);
            } else {
                printf("Attribute: %s not implemented\n", name);
                throw std::invalid_argument("No such attribute implemented!");
            }

            numberOfAttributeItems += attribute.itemSize;
        }
    }

    std::vector<float> vertexData = combineAttributeData(attributesData);
    VertexBuffer vbo{&vertexData[0], static_cast<unsigned int>(vertexData.size() * sizeof(float))};
    vao.addBuffer(vbo, layout);

    if (mesh.geometry.isInstanced) {
        // TODO: Optimization, add all to one VBO then render (later)
        glDrawElements(mesh.drawMode, static_cast<GLsizei>(indices.size()), GL_UNSIGNED_INT, 0);
    } else {
        glDrawArrays(mesh.drawMode, 0, static_cast<GLsizei>(vertexData.size() / numberOfAttributeItems));
    }
    vao.unbind();
    ibo.unbind();
    vbo.unbind();
}

void Renderer::setUniforms(MeshObject &mesh, const Camera &camera, Material &material) const {
    // todo: Pre-process shader source to add chunk: modelView and projectionMatrix (later, now just declare it)
    if (mesh.Object3D::transformChanged) { // Neat syntax for distinguishing from which it is!
        mesh.setTransformMatrix();
    }

    // First set modelView matrix and projectionMatrix for general usage in each shader:
    glm::mat4 modelView = camera.getViewMatrix() * mesh.getModelMatrix();
    glm::mat4 projectionMatrix = camera.getProjectionMatrix();

    // Set uniforms to the material shader
    material.setUniform("projectionMatrix", projectionMatrix);
    material.setUniform("modelViewMatrix", modelView);
    material.setUniformDefault("meshColor", material.color);
    material.setUniformDefault("viewMatrix", camera.getViewMatrix());

    // See which uniforms are active and set them
    GLint i;
    GLint count;

    GLint size; // size of the variable
    GLenum type; // type of the variable (float, vec3 or mat4, etc)

    // todo: do this size properly!
    const GLsizei bufSize = 40; // maximum name length
    GLchar name[bufSize]; // variable name in GLSL
    GLsizei length; // name length


    glGetProgramiv(material.shader.programId, GL_ACTIVE_UNIFORMS, &count);
//    printf("Active Uniforms: %d\n", count);

    for (i = 0; i < count; i++) {
        glGetActiveUniform(material.shader.programId, (GLuint) i, bufSize, &length, &size, &type, name);

//        printf("Uniform #%d Type: %u Name: %s\n", i, type, name);

        std::map<std::string, std::any> uniforms = material.uniforms;
        std::any value = uniforms[name];
        if (material.uniforms.find(name) == material.uniforms.end()) {
            printf("Uniform: %s not found\n", name);
            throw std::invalid_argument("No such uniform found!");
        } else if (type == GL_FLOAT) {
            float ext = std::any_cast<float>(value);
            material.setFloat(name, ext);
        } else if (type == GL_FLOAT_MAT4) {
            glm::mat4 ext = std::any_cast<glm::mat4>(value);
            material.setMat4(name, ext);
        } else if (type == GL_FLOAT_VEC3) {
            glm::vec3 ext = std::any_cast<glm::vec3>(value);
            material.setVec3(name, ext);
        } else if (type == GL_FLOAT_VEC4) {
            glm::vec4 ext = std::any_cast<glm::vec4>(value);
            material.setVec4(name, ext);
        } else if (type == GL_BOOL) {
            bool ext = std::any_cast<bool>(value);
            material.setBool(name, ext);
        } else if (type == GL_INT) {
            int ext = std::any_cast<int>(value);
            material.setInt(name, ext);
        } else if (type == GL_SAMPLER_2D) {
            int ext = std::any_cast<int>(value);
            material.setInt(name, ext);
        } else {
            std::cout << "Not supported type: " << type << std::endl;
            throw std::invalid_argument("No supported uniform!");
        }
    }
}

// Convert this to direct shader impl!
void Renderer::renderParticles(MeshObject &mesh, glm::vec3 position, std::vector<Particle> particles,
                               const Camera &camera) const {
    Material &material = mesh.material;
    material.setUniform("viewMatrix", camera.getViewMatrix());
    material.setUniform("generator_location", position);
    material.setUniform("sprite", 0);
//    std::cout << "Program inside:" << material.shader.programId << std::endl;
    for (Particle &particle : particles) {
        if (particle.life > 0.0) {
            // render particle
            material.setUniform("offset", particle.position);
            material.setUniform("particle_color", particle.color);
            material.setUniform("particle_size", particle.size);
            renderMesh(mesh, camera, material);
        }
    }
}

std::vector<float> Renderer::combineAttributeData(std::vector<BufferAttribute> attributes) const {
    std::vector<float> interleavedData;

    unsigned int vertexCount = attributes[0].data.size() / attributes[0].itemSize;
    for (unsigned int currentVertex = 0; currentVertex < vertexCount; currentVertex++) {
        for (auto &attribute : attributes) {
            for (int c = 0; c < attribute.itemSize; c++) {
                interleavedData.push_back(attribute.data[currentVertex * attribute.itemSize + c]);
            }
        }
    }

    return interleavedData;
}
