#include <openGL/rendering/PerspectiveCamera.hpp>
#include <glm/glm.hpp>
#include <iostream>

PerspectiveCamera::PerspectiveCamera(float fieldOfView, float aspectRatio, float near, float far) : fov(fieldOfView),
                                                                                                    aspectRatio(
                                                                                                            aspectRatio),
                                                                                                    near(near),
                                                                                                    far(far) {

    updateProjectionMatrix();
}

void PerspectiveCamera::updateProjectionMatrix() {
    projectionMatrix = glm::perspective(glm::radians(Zoom), aspectRatio, near, far);
}

glm::mat4 PerspectiveCamera::getProjectionMatrix() const {
    return projectionMatrix;
}

glm::mat4 PerspectiveCamera::getViewMatrix() const {
    return viewMatrix;
}

glm::mat4 PerspectiveCamera::getWorldMatrix() const {
    return worldTransform;
}

void PerspectiveCamera::setAspectRatio(float aspectRatio) {
    this->aspectRatio = aspectRatio;
    updateProjectionMatrix();
}



