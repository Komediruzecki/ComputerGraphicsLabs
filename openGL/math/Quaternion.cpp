#define GLM_ENABLE_EXPERIMENTAL

#include <openGL/math/Quaternion.hpp>

#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>

Quaternion::Quaternion(float s, float x, float y, float z) : s(s), v(glm::vec3(x, y, z)) {
}

Quaternion::Quaternion(glm::vec4 elements) : s(elements.x), v(glm::vec3(elements.y, elements.z, elements.w)) {

}

Quaternion::Quaternion(float s, glm::vec3 elements) : s(s), v(elements) {

}


Quaternion &operator+(const Quaternion &lhs, const Quaternion &rhs) {
    return lhs.copy().add(rhs);
}

Quaternion &operator-(const Quaternion &lhs, const Quaternion &rhs) {
    return lhs.copy().sub(rhs);
}

Quaternion &operator*(const Quaternion &lhs, const Quaternion &rhs) {
    return lhs.copy().mul(rhs);
}

Quaternion &operator*(const Quaternion &lhs, const double scalar) {
    return lhs.copy().scalarMul(scalar);
}

Quaternion &operator*(const double scalar, const Quaternion &lhs) {
    return lhs.copy().scalarMul(scalar);
}

Quaternion &Quaternion::operator+=(const Quaternion &rhs) {
    this->add(rhs);
    return *this;
}

Quaternion &Quaternion::operator-=(const Quaternion &rhs) {
    this->sub(rhs);
    return *this;
}

Quaternion &Quaternion::operator+=(Quaternion &rhs) {
    this->add(rhs);
    return *this;
}

Quaternion &Quaternion::operator-=(Quaternion &rhs) {
    this->sub(rhs);
    return *this;
}

Quaternion &Quaternion::operator=(const Quaternion &other) {
    // Check for self assignment
    if (this != &other) {
        this->s = other.s;
        this->v = glm::vec3(other.v);
    }

    return *this;
}


Quaternion &Quaternion::add(Quaternion q) {
    this->s += q.s;
    this->v -= q.v;
    return *this;
}

Quaternion &Quaternion::sub(Quaternion q) {
    this->s -= q.s;
    this->v -= q.v;
    return *this;
}

Quaternion &Quaternion::mul(Quaternion q) {
    const float sp = this->s * q.s - glm::dot(this->v, q.v);

    const glm::vec3 sab = q.v * this->s;
    const glm::vec3 sba = this->v * q.s;
    const glm::vec3 aCrossB = glm::cross(this->v, q.v);
    const glm::vec3 vecProduct = sab + sba + aCrossB;
    this->s = sp;
    this->v = glm::vec3(vecProduct);
    return *this;
}

void Quaternion::rotate(glm::vec3 point, float angle, glm::vec3 axis) {
    const Quaternion qp = Quaternion(0.0, point);
    Quaternion q = Quaternion::rotation(angle, axis);
    Quaternion result = q * qp * q.inverse();
    this->s = result.s;
    this->v = result.v;
}

Quaternion &Quaternion::rotation(float angle, glm::vec3 axis) {
    const double halfAngle = glm::radians(angle / 2);

    const glm::vec3 vecPart = glm::normalize(axis) * (float) sin(halfAngle);
    Quaternion *q = new Quaternion(static_cast<float>(cos(halfAngle)), vecPart);
    return *q;
};

Quaternion &Quaternion::scalarMul(float s) {
    this->s *= s;
    this->v *= s;
    return *this;
}

Quaternion &Quaternion::square() {
    float oldS = this->s;
    this->s = oldS * oldS - this->v.x * this->v.x
              - this->v.y * this->v.y - this->v.z * this->v.z;
    this->v *= 2 * oldS;
    return *this;
}

double Quaternion::normSquared() const {
    return this->s * this->s + this->v.x * this->v.x +
           this->v.y * this->v.y + this->v.z * this->v.z;
}

double Quaternion::norm() const {
    float result = 0;

    glm::vec4 vec = glm::vec4(this->s, this->v);
    for (int c = 0; c < 4; c++) {
        result += (vec[c] * vec[c]);
    }

    return std::sqrt(result);
}

Quaternion &Quaternion::normalize() {
    double norm = this->norm();
    this->s /= norm;
    glm::normalize(this->v);
    return *this;
}

Quaternion &Quaternion::conjugate() {
    this->v *= -1;
    return *this;
}

Quaternion &Quaternion::inverse() {
    this->conjugate();
    double normSquared = this->normSquared();

    this->s /= normSquared;
    this->v /= normSquared;
    return *this;
}

glm::vec4 Quaternion::getElements() {
    return glm::vec4(this->s, this->v);
}

double Quaternion::getScalar() {
    return this->s;
}

glm::vec3 Quaternion::getVector() {
    return this->v;
}

glm::mat4 Quaternion::toMatrix() const {
    const float w = this->s;
    const float qx = this->v.x;
    const float qy = this->v.y;
    const float qz = this->v.z;
    const glm::mat4 m1 = glm::mat4(
            {
                    {w,   qz,  -qy, qx},
                    {-qz, w,   qx,  qy},
                    {qy,  -qx, w,   qz},
                    {-qx, -qy, -qz, w}
            });
    const glm::mat4 m2 = glm::mat4(
            {
                    {w,   qz,  -qy, -qx},
                    {-qz, w,   qx,  -qy},
                    {qy,  -qx, w,   -qz},
                    {qx,  qy,  qz,  w}
            });

    return m1 * m2;
}

Quaternion &Quaternion::copy() const {
    return *(new Quaternion{this->s, glm::vec3(this->v)});
}

std::ostream &operator<<(std::ostream &stream, const Quaternion &q) {
    stream << "Q(" << q.s << "| " << q.v.x << " " << q.v.y << " " << q.v.z << ")";
    return stream;
}

Quaternion::~Quaternion() = default;


