#include <openGL/buffers/IndexBuffer.hpp>
#include <glad/glad.h>

// todo: If having more than 65k indices, change this to general stuff!
IndexBuffer::IndexBuffer(const unsigned int *data, unsigned int count) : count(count) {
    // Use GLuint not C++ implementation based unsigned int
    static_assert(sizeof(GLuint) == sizeof(unsigned int));
    glGenBuffers(1, &rendererID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), data, GL_STATIC_DRAW);
}

IndexBuffer::~IndexBuffer() {
    glDeleteBuffers(1, &rendererID);
}

void IndexBuffer::bind() const {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererID);
}

void IndexBuffer::unbind() const {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

unsigned int IndexBuffer::getCount() const {
    return count;
}
