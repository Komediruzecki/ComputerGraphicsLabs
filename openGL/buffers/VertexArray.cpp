#include <openGL/buffers/VertexArray.hpp>
#include <openGL/buffers/VertexBufferLayout.hpp>
#include <openGL/buffers/VertexBuffer.hpp>
#include <glad/glad.h>

VertexArray::VertexArray() {
    glCreateVertexArrays(1, &rendererID);
}

VertexArray::~VertexArray() {
    glDeleteVertexArrays(1, &rendererID);
}

void VertexArray::addBuffer(const VertexBuffer &vb, const VertexBufferLayout &layout) {
    bind();
    vb.bind();
    const auto &elements = layout.getElements();

    unsigned int offset = 0;
    for (unsigned int c = 0; c < elements.size(); c++) {
        const auto &element = elements[c];
        glEnableVertexAttribArray(c);
        glVertexAttribPointer(c,
                              element.count, element.type, element.normalized,
                              layout.getStride(), (const void *) offset);
        offset += element.count * VertexBufferElement::getSizeOfType(element.type);
    }
}

void VertexArray::bind() const {
    glBindVertexArray(rendererID);
}

void VertexArray::unbind() const {
    glBindVertexArray(0);
}
