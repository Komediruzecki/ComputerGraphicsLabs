#include <openGL/objects/BSpline.hpp>
#include <fstream>
#include <glm/ext.hpp>

#include <vector>
#include <glm/glm.hpp>

#include <openGL/rendering/Shader.hpp>
#include <openGL/objects/materials/Material.hpp>
#include <openGL/buffers/VertexArray.hpp>
#include <openGL/buffers/VertexBuffer.hpp>
#include <openGL/buffers/VertexBufferLayout.hpp>
#include <openGL/buffers/IndexBuffer.hpp>
#include <openGL/objects/geometry/BufferGeometry.hpp>
#include <openGL/objects/geometry/BufferAttribute.hpp>
#include <openGL/rendering/ShaderUtil.hpp>

// Instancing Stuff -> Make framework for that
// Pretty much simple: Just set divisor, set new attribute which differ for each instance, and call drawInstanced version.
BSpline::BSpline(std::vector<glm::vec3> controlPoints, glm::mat4 periodicSegmentMatrix) : controlPoints(controlPoints),
                                                                                          periodicSegmentMatrix(
                                                                                                  periodicSegmentMatrix),
                                                                                          MeshObject(
                                                                                                  createGeometry(
                                                                                                          controlPoints,
                                                                                                          periodicSegmentMatrix,
                                                                                                          0.01),
                                                                                                  getDefaultMaterial()) {
    drawMode = GL_LINES;
}

Material &BSpline::getDefaultMaterial() {
    Material *m = new Material(ShaderUtil::createMaterialFromShaders("./assets/shaders/basic/vertex.vert",
                                                                     "./assets/shaders/basic/pixel.frag"));
    return *m;
}


std::vector<unsigned int> BSpline::makeLineIndices(int numberOfPositions) {
    std::vector<unsigned int> indices;
    for (int c = 0; c < numberOfPositions - 1; c++) {
        indices.push_back(c);
        indices.push_back(c + 1);
    }

    return indices;
}

BufferGeometry &BSpline::createGeometry(const std::vector<glm::vec3> controlPoints, glm::mat4 periodicSegmentMatrix,
                                        float granularity) {
    std::vector<float> positions;
    for (int c = 1; c <= controlPoints.size() - 3; c++) {
        for (float t = 0.0; t < 1.0; t += granularity) {
//             Just generate points; See later how to draw a general function given some granularity in Shaders i.e. openGL
            glm::vec4 result = glm::vec4(evaluate(controlPoints, periodicSegmentMatrix, t, c), 1.0f);
            positions.push_back(result.x);
            positions.push_back(result.y);
            positions.push_back(result.z);
        }
    }

    BufferGeometry *geom = new BufferGeometry(true);
    BufferAttribute positionAttribute = BufferAttribute(positions, 3);
    geom->addAttribute("vertexPosition", positionAttribute);
    geom->setIndices(makeLineIndices(positions.size() / 3));

    return *geom;
}

/** Static functions */
BSpline &BSpline::fromFile(const std::string &filePath) {
    // Read control points
    std::vector<glm::vec3> controlPoints;
    std::ifstream file(filePath);

    int x, y, z;
    while (file >> x >> y >> z) {
        controlPoints.push_back(glm::vec3(x, y, z));
    }

    // Generate BSpline
    float elementsB[16] = {
            -1, 3, -3, 1,
            3, -6, 3, 0,
            -3, 0, 3, 0,
            1, 4, 1, 0};

    glm::mat4 periodicSegmentMatrix = glm::make_mat4x4(elementsB);
    periodicSegmentMatrix = glm::transpose(periodicSegmentMatrix);
    BSpline *spline = (new BSpline(controlPoints, periodicSegmentMatrix));
    return *spline;
}

glm::vec3 BSpline::evaluate(float t, int segment) {
    return evaluate(this->controlPoints, this->periodicSegmentMatrix, t, segment);
}

glm::vec3 BSpline::evaluateSecondDer(float t, int seg) {
    glm::mat4 matrixR = getControlPointsMatrix(seg);
    glm::vec4 secondDerPoint = glm::vec4(6 * t, 2, 0, 0);
    glm::vec3 secondDerivative = (secondDerPoint * (periodicSegmentMatrix * matrixR));
    return secondDerivative;
}

glm::vec3 BSpline::evaluateFirstDer(float t, int seg) {
    // Fourth row = 0,0,0,0, or 0,0,0,1
    // Not generic, this is for default periodic matrix!
    float elements[16] = {
            -1, 3, -3, 1,
            2, -4, 2, 0,
            -1, 0, 1, 0,
            0, 0, 0, 1};

    glm::mat4 matrixR = getControlPointsMatrix(seg);
    glm::mat4 periodicDerivative = glm::make_mat4(elements);
    periodicDerivative = glm::transpose(periodicDerivative);

    glm::vec4 firstDerPoint = glm::vec4(t * t, t, 1, 0);
    glm::vec3 firstDerivative = ((firstDerPoint * (periodicDerivative * matrixR))) * 0.5f;
    return firstDerivative;
}

glm::mat4 BSpline::getControlPointsMatrix(int seg) {
    return getControlPointsMatrix(this->controlPoints, seg);
}

std::vector<glm::vec3> BSpline::getControlPoints() {
    return controlPoints;
}

glm::vec3
BSpline::evaluate(const std::vector<glm::vec3> &controlPoints, glm::mat4 periodicSegmentMatrix, float t, int segment) {
    glm::mat4 matrixR = getControlPointsMatrix(controlPoints, segment);

    float oneSixth = 1.0f / 6.0f;
    glm::vec4 vectorT = glm::vec4(t * t * t, t * t, t, 1);
    glm::vec4 result = (vectorT * (periodicSegmentMatrix * matrixR)) * oneSixth;
    return glm::vec3(result);
}

glm::mat4 BSpline::getControlPointsMatrix(std::vector<glm::vec3> controlPoints, int seg) {
    float elementsR[16] = {
            controlPoints[seg - 1].x,
            controlPoints[seg - 1].y,
            controlPoints[seg - 1].z,
            0.0,
            controlPoints[seg].x,
            controlPoints[seg].y,
            controlPoints[seg].z,
            0.0,
            controlPoints[seg + 1].x,
            controlPoints[seg + 1].y,
            controlPoints[seg + 1].z,
            0.0,
            controlPoints[seg + 2].x,
            controlPoints[seg + 2].y,
            controlPoints[seg + 2].z,
            1.0};
    glm::mat4 matrixR = glm::make_mat4(elementsR);
    matrixR = glm::transpose(matrixR);

    return matrixR;
}
