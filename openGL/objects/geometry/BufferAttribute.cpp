#include <openGL/objects/geometry/BufferAttribute.hpp>

#include<vector>
#include <utility>


BufferAttribute::BufferAttribute(std::vector<float> data, unsigned int itemSize) : itemSize(itemSize), data(
        std::move(data)) {}

const BufferAttribute &BufferAttribute::copy() const {
    BufferAttribute *attribute = new BufferAttribute(std::vector<float>(data), itemSize);
    return *attribute;
}

BufferAttribute::BufferAttribute() : itemSize(0), data(std::vector<float>(2)) {

}
