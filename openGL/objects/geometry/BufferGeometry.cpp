#include <openGL/objects/loaders/OBJ_Loader.hpp>
#include <openGL/objects/geometry/BufferGeometry.hpp>
#include <openGL/objects/geometry/BufferAttribute.hpp>

#include <utility>
#include <vector>


BufferGeometry::BufferGeometry() = default;

void BufferGeometry::addAttribute(const std::string &name, const BufferAttribute &attribute) {
    attributes[name] = attribute.copy();
}

BufferGeometry &BufferGeometry::fromLoader(const objl::Loader loader) {
    objl::Mesh mesh = loader.LoadedMeshes[0];
    std::vector<float> positions;
    std::vector<float> normals;
    std::vector<float> textures;
    for (int i = 0, size = mesh.Vertices.size(); i < size; i++) {
        positions.push_back(mesh.Vertices[i].Position.X);
        positions.push_back(mesh.Vertices[i].Position.Y);
        positions.push_back(mesh.Vertices[i].Position.Z);
        normals.push_back(mesh.Vertices[i].Normal.X);
        normals.push_back(mesh.Vertices[i].Normal.Y);
        normals.push_back(mesh.Vertices[i].Normal.Z);
        textures.push_back(mesh.Vertices[i].TextureCoordinate.X);
        textures.push_back(mesh.Vertices[i].TextureCoordinate.Y);
    }

    std::vector<unsigned int> indices{};
    for (int i = 0, size = mesh.Indices.size(); i < size; i++) {
        indices.push_back(mesh.Indices[i]);
    }

    BufferGeometry *geom = new BufferGeometry(true);
    BufferAttribute positionAttribute = BufferAttribute(positions, 3);
    BufferAttribute normalAttribute = BufferAttribute(normals, 3);
    BufferAttribute textureAttribute = BufferAttribute(textures, 2);
    geom->addAttribute("vertexPosition", positionAttribute);
    geom->addAttribute("vertexNormal", normalAttribute);
    geom->addAttribute("texCoords", textureAttribute);
    geom->setIndices(indices);
    return *geom;
}

void BufferGeometry::setIndices(std::vector<unsigned int> vector) {
    indices = std::move(vector);
}

std::vector<unsigned int> BufferGeometry::getIndices() const {
    return indices;
}

BufferGeometry &BufferGeometry::copy() const {
    BufferGeometry *geom = new BufferGeometry(isInstanced);

    for (const auto &[key, value]: attributes) {
        // Add attribute copies value (data)
        geom->addAttribute(key, value);
    }

    geom->setIndices(std::vector<unsigned int>(getIndices()));
    return *geom;
}

BufferGeometry::BufferGeometry(bool instanced) : isInstanced(instanced) {

}
