#include<openGL/objects/scene-graph/SceneNode.hpp>
#include<openGL/rendering/Renderer.hpp>
#include<openGL/objects/MeshObject.hpp>

#include<vector>
#include<openGL/objects/scene-graph/SceneNode.hpp>
#include<openGL/objects/materials/Material.hpp>
#include<openGL/EngineCore.hpp>

static unsigned int idCounter = 0;

SceneNode::SceneNode(MeshObject *mesh) : mesh(mesh), parent(nullptr),
                                         modelScale(glm::vec3(1, 1, 1)) {
    if (mesh) {
        // todo: fix this somehow to have 1 shader but multiple materials
        material = &mesh->material.copy();
    }

    transform = glm::mat4(1.0f);
    worldTransform = glm::mat4(1.0f);

    idCounter += 1;
    id = "Root-" + std::to_string(idCounter);
    uid = idCounter;
}


SceneNode::~SceneNode() {
    for (auto &c : children) {
        delete c;
    }
}

void SceneNode::addChild(SceneNode *node) {
    if (node == this) {
        return;
    }
    children.push_back(node);
    node->parent = this;
}

const glm::mat4 &SceneNode::getWorldTransform() const {
    return worldTransform;
}

const glm::mat4 &SceneNode::getTransform() const {
    return transform;
}

const glm::vec3 &SceneNode::getModelScale() const {
    return modelScale;
}

MeshObject *SceneNode::getMesh() const {
    return mesh;
}

void SceneNode::setTransform(const glm::mat4 &transform) {
    this->transform = transform;
}

void SceneNode::setModelScale(const glm::vec3 &modelScale) {
    this->modelScale = modelScale;
}

void SceneNode::setMesh(MeshObject *mesh) {
    this->mesh = mesh;
}

std::vector<SceneNode *>::const_iterator SceneNode::getChildIteratorStart() {
    return children.begin();
}

std::vector<SceneNode *>::const_iterator SceneNode::getChildIteratorEnd() {
    return children.end();
}

void SceneNode::draw(const Renderer &renderer, const Camera &camera) {
    if (mesh) {
        renderer.renderMesh(*mesh, camera, *material);
    }
}

void SceneNode::update(float ms) {
    if (parent) {
        this->worldTransform = parent->worldTransform * transform;
    } else {
        worldTransform = transform;
    }

    for (auto &i : children) {
        i->update(ms);
    }
}

void SceneNode::removeChild(SceneNode *node) {
    throw std::invalid_argument("Not implemented: removing from scene graph!");
}

Material &SceneNode::getMaterial() const {
    return *material;
}

std::string SceneNode::toString() const {
    return "id: " + this->id;
}

void SceneNode::setId(const std::string &id) {
    SceneNode::id = id;
}

void SceneNode::setMaterial(Material *material) {
    SceneNode::material = material;
}
