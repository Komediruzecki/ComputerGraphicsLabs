#include<openGL/objects/scene-graph/CubeRobot.hpp>
#include<openGL/objects/MeshObject.hpp>
#include <openGL/objects/scene-graph/CubeRobot.hpp>
#include<openGL/objects/materials/Material.hpp>
#include <openGL/math/matrix/MatrixUtil.hpp>


MeshObject *CubeRobot::cube = nullptr;

CubeRobot::CubeRobot() {
    // Local Origin Marker!
//    if (cube) {
//        setMesh(cube);
//        transform = glm::mat4(1.0f);
//        material = &cube->material.copy();
//        material->setUniform("meshColor", glm::vec3(1, 1, 1));
//    }

    id += "-Robot";
    // Body
    SceneNode *body = new SceneNode(cube);
    body->setId("Body");
    body->getMaterial().setUniform("meshColor", glm::vec3(1, 0, 0));
    body->setModelScale(glm::vec3(10, 15, 5));
    body->setTransform(MatrixUtil::translation(glm::vec3(0, 35, 0)));
    addChild(body);

    // Head
    head = new SceneNode(cube);
    head->setId("Head");
    head->getMaterial().setUniform("meshColor", glm::vec3(0, 1, 0));
    head->setModelScale(glm::vec3(5, 5, 5));
    head->setTransform(MatrixUtil::translation(glm::vec3(0, 30, 0)));
    body->addChild(head);


    // LeftArm
    leftArm = new SceneNode(cube);
    leftArm->setId("LeftArm");
    leftArm->getMaterial().setUniform("meshColor", glm::vec3(0, 0, 1));
    leftArm->setModelScale(glm::vec3(3, -18, 3));
    leftArm->setTransform(MatrixUtil::translation(glm::vec3(-12, 30, -1)));
    body->addChild(leftArm);

    // RightArm
    rightArm = new SceneNode(cube);
    rightArm->setId("RightArm");
    rightArm->getMaterial().setUniform("meshColor", glm::vec3(0, 0, 1));
    rightArm->setModelScale(glm::vec3(3, -18, 3));
    rightArm->setTransform(MatrixUtil::translation(glm::vec3(12, 30, -1)));
    body->addChild(rightArm);

    SceneNode *leftLeg = new SceneNode(cube);
    leftLeg->setId("LeftLeg");
    leftLeg->getMaterial().setUniform("meshColor", glm::vec3(0, 0, 1));
    leftLeg->setModelScale(glm::vec3(3, -17.5, 3));
    leftLeg->setTransform(MatrixUtil::translation(glm::vec3(-8, 0, 0)));
    body->addChild(leftLeg);

    SceneNode *rightLeg = new SceneNode(cube);
    rightLeg->setId("RightLeg");
    rightLeg->getMaterial().setUniform("meshColor", glm::vec3(0, 0, 1));
    rightLeg->setModelScale(glm::vec3(3, -17.5, 3));
    rightLeg->setTransform(MatrixUtil::translation(glm::vec3(8, 0, 0)));
    body->addChild(rightLeg);
}

void CubeRobot::update(float ms) {
    float mSec = ms * 1000;
    float div = 5.0f;
    transform = glm::rotate(transform, ms / div, glm::vec3(0, 1, 0));

    head->setTransform(glm::rotate(head->getTransform(), -ms / div, glm::vec3(0, 1, 0)));
    leftArm->setTransform(glm::rotate(leftArm->getTransform(), -ms / div, glm::vec3(1, 0, 0)));
    rightArm->setTransform(glm::rotate(rightArm->getTransform(), ms / div, glm::vec3(1, 0, 0)));

    SceneNode::update(ms);
}

