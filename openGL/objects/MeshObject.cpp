#include <openGL/objects/MeshObject.hpp>
#include <openGL/objects/geometry/BufferGeometry.hpp>
#include <openGL/objects/materials/Material.hpp>
#include <openGL/objects/RenderType.hpp>

MeshObject::MeshObject(BufferGeometry &geometry, Material &material) : drawMode(GL_TRIANGLES),
                                                                       geometry(geometry),
                                                                       material(material) {

}

RenderType MeshObject::getType() const {
    return RenderType::MESH_OBJECT;
}

MeshObject::~MeshObject() = default;


