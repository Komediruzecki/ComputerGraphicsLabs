#include <openGL/objects/materials/BasicMeshMaterial.hpp>
#include <openGL/rendering/Shader.hpp>
#include <openGL/rendering/ShaderUtil.hpp>

BasicMeshMaterial::BasicMeshMaterial(Shader &shader) : Material(shader) {}

BasicMeshMaterial::BasicMeshMaterial() : Material(getShader()) {
}

const Shader &BasicMeshMaterial::getShader() {
    std::string vertexShader = ShaderUtil::readShaderFromFile("./assets/shaders/basic_mesh_shader/vertex.vert");
    std::string fragmentShader = ShaderUtil::readShaderFromFile("./assets/shaders/basic_mesh_shader/pixel.frag");
    const Shader *sh = new Shader{vertexShader, fragmentShader};
    return *sh;
}

