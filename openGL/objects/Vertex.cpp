#include <openGL/objects/Vertex.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

Vertex::Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 texCoord) : position(position), normal(normal),
                                                                           texCoord(texCoord) {
    this->vertexData = {position.x, position.y, position.z, normal.x, normal.y, normal.z, texCoord.x, texCoord.y};
}

Vertex::Vertex(glm::vec3 position) : Vertex(position, glm::vec3(0, 0, 0), glm::vec2(0, 0)) {
}
