
#include <openGL/objects/Scene.hpp>

#include "openGL/objects/Scene.hpp"

// Should I include std::vector since its only called by this!

Scene::Scene() = default;

std::vector<Object3D *> Scene::getObjects() const {
    return objects;
}

void Scene::add(Object3D *obj) {
    this->objects.push_back(obj);
}

RenderType Scene::getType() const {
    return RenderType::SCENE;
}

Scene::~Scene() {
    for (auto &object : objects) {
        delete object;
    }
}

