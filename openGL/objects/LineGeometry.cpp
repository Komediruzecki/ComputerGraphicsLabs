#include <openGL/objects/geometry/LineGeometry.hpp>
#include <openGL/objects/geometry/BufferAttribute.hpp>
#include <glm/glm.hpp>

LineGeometry::LineGeometry(const glm::vec2 &startPoint, const glm::vec2 &endPoint) {
    glm::vec2 ts;
    glm::vec2 te;

    // Assume start is always smaller than end If not swap
    if (startPoint.x > endPoint.x || startPoint.y > endPoint.y) {
        te = glm::vec2(startPoint);
        ts = glm::vec2(endPoint);
    } else {
        ts = glm::vec2(startPoint);
        te = glm::vec2(endPoint);
    }

    float thickness = 0.1;
    float halfThickness = thickness / 2;
    glm::vec2 d = te - ts;
    glm::normalize(d);

    glm::vec2 topLeft;
    glm::vec2 bottomLeft;
    glm::vec2 topRight;
    glm::vec2 bottomRight;

    if (d.x == 0) {
        topLeft = glm::vec2(ts.x - halfThickness, ts.y);
        bottomLeft = glm::vec2(ts.x + halfThickness, ts.y);

        topRight = glm::vec2(te.x - halfThickness, te.y);
        bottomRight = glm::vec2(te.x + halfThickness, te.y);
    } else if (d.y == 0) {
        topLeft = glm::vec2(ts.x, ts.y - halfThickness);
        bottomLeft = glm::vec2(ts.x, ts.y + halfThickness);

        topRight = glm::vec2(te.x, te.y - halfThickness);
        bottomRight = glm::vec2(te.x, te.y + halfThickness);
    }

    // Some slope
    float slope = d.y / d.x;
    float normalSlope = -1.0f / slope;

    float bns = ts.y - normalSlope * ts.x;
    float bne = te.y - normalSlope * te.x;
    float dx = (halfThickness * halfThickness) / (normalSlope + 1);
    if (slope > 0.0) {
        topLeft = glm::vec2(ts.x, normalSlope * (ts.x - dx) + bns);
        bottomLeft = glm::vec2(ts.x, normalSlope * (ts.x + halfThickness) + bns);

        topRight = glm::vec2(ts.x, normalSlope * (ts.x - dx) + bne);
        bottomRight = glm::vec2(ts.x, normalSlope * (ts.x + halfThickness) + bne);
    } else {
        topLeft = glm::vec2(ts.x, normalSlope * (ts.x + dx) + bns);
        bottomLeft = glm::vec2(ts.x, normalSlope * (ts.x - halfThickness) + bns);

        topRight = glm::vec2(ts.x, normalSlope * (ts.x + dx) + bne);
        bottomRight = glm::vec2(ts.x, normalSlope * (ts.x - halfThickness) + bne);
    }


    std::vector<float> data = {
            bottomLeft.x, bottomLeft.y, 0,
            topRight.x, topRight.y, 0,
            topLeft.x, topLeft.y, 0,

            bottomLeft.x, bottomLeft.y, 0,
            topRight.x, topRight.y, 0,
            bottomRight.x, bottomRight.y, 0
    };

//    for (auto value : data) {
//        std::cout << "Data: " << value << std::endl;
//    }

    // todo: later resolve with its own shader, or leave it at 3 coords?
    BufferAttribute positions = BufferAttribute(data, 3);
    addAttribute("vertexPosition", positions);
}
