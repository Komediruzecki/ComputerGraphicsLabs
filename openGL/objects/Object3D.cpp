#define GLM_ENABLE_EXPERIMENTAL

#include <openGL/objects/Object3D.hpp>
#include <openGL/math/matrix/MatrixUtil.hpp>
#include <openGL/math/Quaternion.hpp>
#include <glm/glm.hpp>
#include <iostream>
#include <glm/gtx/string_cast.hpp>

Object3D::Object3D() : position(glm::vec3(0, 0, 0)), scale(glm::vec3(1, 1, 1)),
                       rotation(
                               Quaternion{
                                       1,
                                       0,
                                       0,
                                       0}
                       ), transformMatrix(glm::mat4(1.0f)) {
}


void Object3D::translate(glm::vec3 translation) {
    setPosition(position + translation);
}

void Object3D::multiplyScale(glm::vec3 scaler) {
    setScale(scale * scaler);
}

void Object3D::setPosition(const glm::vec3 &newPosition) {
    this->position = glm::vec3(newPosition);
    transformChanged = true;
}

void Object3D::setScale(const glm::vec3 &newScale) {
    this->scale = glm::vec3(newScale);
    transformChanged = true;
}

void Object3D::setRotation(const Quaternion &q) {
    this->rotation = q.copy();
    transformChanged = true;
}


void Object3D::setTransformMatrix(glm::mat4 newTransform) {
    transformMatrix = newTransform;
    resetSimpleTransforms();
}

void Object3D::setTransformMatrix() {
    // v = T * S * R * v (the final resulting vec)
    transformMatrix = glm::mat4(1.0);
    transformMatrix *= MatrixUtil::translation(this->position);
    transformMatrix *= MatrixUtil::scale(this->scale);
    transformMatrix *= rotation.toMatrix();
    transformChanged = false;

//    std::cout << "Transfomr: " << glm::to_string(transformMatrix) << std::endl;
}

glm::mat4 Object3D::getModelMatrix() const {
    return transformMatrix;
}

void Object3D::rotate(float angleDegrees, glm::vec3 axis) {
    Quaternion q = Quaternion::rotation(angleDegrees, axis);

    // Multiply in reverse order (to get correct rotation)
    setRotation(q.copy().mul(rotation));
}

void Object3D::resetSimpleTransforms() {
    position = glm::vec3(0, 0, 0);
    scale = glm::vec3(1, 1, 1);
    rotation = Quaternion(1, 0, 0, 0);
}

void Object3D::setRotation(float angle, glm::vec3 axis) {
    Quaternion q = Quaternion::rotation(angle, axis);

    // Multiply in reverse order (to get correct rotation)
    setRotation(q);
}

Object3D::~Object3D() = default;

